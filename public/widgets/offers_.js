(function() {
  var chart, highcharts_chart;

  chart = {};

  highcharts_chart = {};

  $ = jQuery;

  $(document).ready(function() {

    var income_axis, income_series;

    if (typeof price_values !== "undefined" && price_values !== null) {
      console.info({
        file: 'offers.js',
        price_values: price_values
      });
      highcharts_chart = {
        chart: {
          renderTo: "mppl_chart",
          type: "areaspline"
        },
        title: {
          text: translations.title
        },
        xAxis: {
          title: {
            text: translations.people
          },
          allowDecimals: false
        },
        yAxis: [
          {
            plotLines: [
              {
                color: "#FFFF00",
                width: 2,
                value: 0
              }
            ],
            title: {
              text: translations.price
            }
          }
        ],
        tooltip: {
          formatter: function() {
            return "<b>People:</b> " + this.x + "<br><b>" + this.series.name + "</b>" + ': ' + this.y;
          }
        },
        plotOptions: {
          areaspline: {
            fillOpacity: 0.3
          }
        },
        series: [
          {
            name: "Price",
            data: price_values,
            type: "spline"
          }
        ]
      };
      if (income.length > 0) {

        income_axis = {
          min: min_income,
          max: max_income,
          title: {
            text: translations.ncome
          },
          opposite: true,
          plotLines: [
            {
              color: "#FF0000",
              width: 2,
              value: 0
            }
          ]
        };
        income_series = {
          name: "Income",
          data: income,
          yAxis: 1
        };
        highcharts_chart.yAxis.push(income_axis);
        highcharts_chart.series.push(income_series);

      }

    }
    if ((typeof Highcharts !== "undefined" && Highcharts !== null) && (typeof price_values !== "undefined" && price_values !== null)) {

      console.info("highcharts_chart");
      console.info(highcharts_chart);

      chart = new Highcharts.Chart(highcharts_chart);
      $('.highcharts-legend').hide();
      return chart;
    } else {
      if(typeof Highcharts == "undefined"){
        console.info('typeof Highcharts == "undefined"');
      }
      if(Highcharts == null){
        console.info('Highcharts == null');
      }
      if(typeof price_values == "undefined"){
        console.info('typeof price_values == "undefined"');
      } else if(price_values == null){
        console.info('price_values == null');
      }
    }
  });

}).call(this);
