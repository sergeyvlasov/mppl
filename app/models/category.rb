class Category < ActiveRecord::Base
  has_many :offers
  belongs_to :parent, class_name: 'Category'
  has_many :children, class_name: 'Category', foreign_key: 'parent_id'
  attr_accessible :name, :parent_id

    #TODO:
    # ensure there are model/controller specs for categories
    # refactor category parent checking into the category model
    # refactor to:
    #   controller: if/unless category.has_parent?
    #                 ...
    #   model: def has_parent?
    #            parent_id
    #          end        

  def find_offers category
  	
    if category.parent_id == 0 || category.parent_id = nil
      children = Category.where("parent_id = ?", category.id)

      num_children = 0
      children.each do
        num_children = num_children + 1
      end

      return offers = Offer.published.where(:category_id => (children[0][:id])..(children[num_children-1][:id]))
    
    else
      return offers = Offer.published.where("category_id = ? ", category)
  	end
  end
end