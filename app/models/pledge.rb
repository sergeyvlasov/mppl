require 'securerandom'

class Pledge < ActiveRecord::Base

  #TODO: (1) align the case (2) refactor to something else(?)
  PAYMENT_STATUSES = [ :na, :cash, :pledged, :canceled ]
  PAYMENT_STATUS_STRINGS = {
    na: "N/A",
    cash: "Cash",
    pledged: "Pledged",
    canceled: "Canceled"
  }


  belongs_to :user
  belongs_to :offer
  has_one :payment

  validates_presence_of :amount
  validates_numericality_of :amount, greater_than_or_equal_to: 1.0

  validate :user_id_for_cash
  validates :offer_id, :presence => {:message => 'Offer cannot be blank'}
  validates_inclusion_of :payment_status, :in => PAYMENT_STATUSES

  attr_accessible :user_id, :offer_id, :amount, :preapproval_key, :payment_status, :created_at #, :status
  attr_readonly :auth

  before_create :create_uniq_key

  # before_update :authenticate
  # before_destroy :authenticate
  scope :available, -> { order(:created_at).where payment_status: [:cash, :pledged].map { |s| PAYMENT_STATUSES.index(s)  } }
  #default_scope available
  
  scope :pledged, -> { where payment_status: PAYMENT_STATUSES.index(:pledged) }
  scope :not_available, -> { where payment_status: PAYMENT_STATUSES.index(:na) }

  scope :recent, ->(since = 1.day.ago) { available.where(arel_table[:created_at].gt(since)) }
  scope :aged, ->(since = 1.day.ago) { available.where(arel_table[:created_at].lt(since)) }

  scope :payable, ->(day = Date.today) do
    where(payment_status: PAYMENT_STATUSES.index(:pledged)) \
    .joins(:offer).where("offers.payment_collection_date <= :day", day: day) \
    .includes(:payment).where("payments.id" => nil)
  end

  def self.ps_idx payment_status
    return nil until payment_status
    return payment_status.map { |el| _ps_idx el }  if payment_status.is_a? Array
    return _ps_idx payment_status
  end

  def self._ps_idx payment_status 
    PAYMENT_STATUSES.index(payment_status.to_sym)
  end
  
  #TODO: refactor it a dedicated class?
  def activate old_pledge = nil, sender_email = nil
    #TODO: send pledge upgrade email notification
    if old_pledge && old_pledge.payment_status != :canceled
      old_pledge.cancel
    end

    #ensure there is a pledger
    if(!self.user)
      if(sender_email)
        user = add_user(sender_email) #if (sender_email && !self.user)
        
      else
       ### ???
      end
    end
    if payment_status == :na
      update_attribute :payment_status, :pledged
      
      NotificationMailer.successful_pledge_buyer(self).deliver
      NotificationMailer.successful_pledge_seller(self).deliver
    end
  end

  def self.digestable_pledges
    Pledge.where(offer_id: Offer.recently_pledged_offer_ids) - Pledge.recent
  end

 

  def payment_status_str
    PAYMENT_STATUS_STRINGS[payment_status]
  end

  

  def payment_status
    PAYMENT_STATUSES[read_attribute(:payment_status)]
  end

  def payment_status= status
    #might be a string instead of symbol
    status = status.to_sym
    write_attribute(:payment_status, PAYMENT_STATUSES.index(status))
  end

  def self.to_csv
      CSV.generate do |csv|
        csv << column_names
          all.each do |pledge|
            csv << pledge.attributes.values_at(*column_names)
      end
    end
  end

  def status

    if(self.offer.active? && self.amount >= self.offer.current_offer_price)
      :active
    else
      :pending
    end

  end

  def available?
    payment_status.in? [:cash, :pledged]
  end

  def pledged?
    payment_status == :pledged
  end

  def canceled?
    payment_status == :canceled
  end

  def cancelled?
    canceled?
  end

 

  def payable?
    active? && payment_status == :pledged && payment.nil? 
  end

  def ipns
    payment_notifications
  end

  def payment_notifications
    PaymentNotification.where(preapproval_key: preapproval_key)
  end

  def active?
    self.status == :active
  end

  def pending?
    self.status == :pending
  end

  def status_str
    case self.status
    when :pending
      "Pending"
    when :active
      "Active"
    end
  end

  def authenticate(other_auth)
  	!other_auth.nil? && auth == other_auth
  end

  def self.delete_previous_pledge_if_exists(params)
    if (!params[:pledge_id].nil? && !params[:auth].nil?)
      pledge = Pledge.find(params[:pledge_id])
      if !pledge.nil?
        Pledge.destroy(params[:pledge_id]) if pledge.authenticate(params[:auth])
      end
    end
  end

  def cancel paypal = true
    
    if(self.preapproval_key)
      pay_request = PaypalAdaptive::Request.new
      data = {
        preapprovalKey: self.preapproval_key,
        requestEnvelope: {
          errorLanguage: "en_US"
        }
      }
      response = pay_request.cancel_preapproval(data) if paypal
    end
    
    self.update_attribute(:payment_status, :canceled)
  end

  def ppa_request
    @ppa_request ||= PaypalAdaptive::Request.new
  end

  def preapproved_amount
    @preapproved_amount ||= (paypal_preapproval_details["maxTotalAmountOfAllPayments"]).to_f
    @preapproved_amount = self.amount if @preapproved_amount.nil? || @preapproved_amount == 0
    @preapproved_amount
  end

  def pd force = false
    paypal_preapproval_details force
  end

  def paypal_preapproval_details force = false
    fetch_details = -> { ppa_request.preapproval_details( "requestEnvelope"=>{"errorLanguage"=>"en_US"}, "preapprovalKey"=>self.preapproval_key ) }
    if force
      @preapproval_details = fetch_details.call
    else
      @preapproval_details ||= fetch_details.call
    end
  end

  def collect_payment amount = self.amount
    if amount > self.amount
      return false
    end
    puts "going to collect #{amount}"
    ppac ||= PPAClient.new
    ppac.collect_pledge_payment self, amount
  end

  private

  def add_user paypal_email

    begin
      #find or create a user by paypal_email
      user =  User.find_by_paypal(paypal_email) 
      if !user
        user = User.find_by_email(paypal_email) 
      end
      if !user
        user = User.create_by_email(paypal_email, self)
      end
      #associate the user with the pledge
      self.update_attributes user_id: user.id
      #user.pledges << self
      
    rescue Exception => e  
  puts e.message  
  puts e.backtrace.inspect  

      Rails.logger.error("failed to create a user: #{paypal_email}")
    end
    user
  end

  def create_uniq_key
  	self.auth = SecureRandom.uuid
  end

  def user_id_for_cash
    if(self.payment_status == :cash)
      !user_id.nil?
    else
      true
    end
  end


end