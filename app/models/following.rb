class Following < ActiveRecord::Base
  belongs_to :user
  belongs_to :offer
  attr_accessible :offer, :user, :offer_id, :user_id
  validates :offer_id, :uniqueness => { :scope => :user_id }

  def self.find_by_offer_and_user offer, user
    found = where(offer_id: offer.try(:id), user_id: user.try(:id)).first
    if(found)
      return found
    else
      return Following.new
    end
  end
end
