class User < ActiveRecord::Base
  has_many :offers
  has_many :pledges, :dependent => :destroy
  has_many :pledged_offers, through: :pledges, source: :offer
  #has_many :pledges, through: :offers, as: :offer_pledges

  has_many :followings, :dependent => :destroy
  has_many :followed_offers, through: :followings, source: :offer

	rolify
  # Include default devise modules. Others available are:
  # :token_authenticatable, :encryptable, :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  # Setup accessible (or protected) attributes for your model
  attr_accessible :name, :email, :password, :password_confirmation, :remember_me, :paypal
  # attr_accessible :title, :body

  scope :sellers, -> { joins(:offers).uniq }
  scope :with_aged_pledges, -> { joins(:pledges).merge(Pledge.aged).group(:id) }

  def offer_pledges
    Pledge.where(:offer_id => offers.map { |o| o.id }).all
  end

  def follow offer
    followed_offers << offer
  end

  def unfollow offer
    Following.where(offer_id: offer.id, user_id: id).destroy_all
  end

  def follows? offer
    followed_offers.include? offer
  end

  def pledged? offer
    #TODO - need to check if payment_status is 1 or 2 
    !self.pledges.where("offer_id = ? and payment_status = ?", offer, 2).empty?
  end

  def self.create_by_email email, pledge
      password = KeePass::Password.generate('h{8}')
      
      user = User.create!(name: "", email: email, password: password, password_confirmation: password)
      
      user.update_attribute(:name, user.email[0...user.email.index('@')])
      
      NotificationMailer.account_creation(user, password).deliver

      user
  end
end
