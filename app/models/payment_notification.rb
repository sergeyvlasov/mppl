class PaymentNotification < ActiveRecord::Base
  attr_accessible :params, :preapproval_key, :approved, :status, :sender_email
  default_scope -> { order("id desc").limit(10) }
  def verify
  	ppc = PaypalClient.new
  	params = eval(self.params)
  	ppc.verify_ipn params
  end
end
