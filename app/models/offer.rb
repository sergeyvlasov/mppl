class Offer < ActiveRecord::Base

  STATUS_ACTIVE = :active
  STATUS_PENDING = :pending

  resourcify

  belongs_to :user
  belongs_to :category
  has_many :pledges, :dependent => :destroy
  has_many :pledgers, through: :pledges, source: :user
  has_many :payments, through: :pledges

  has_many :followings
  has_many :followers, through: :followings, source: :user

  attr_accessible :user_id, :category_id, :details, :max_people, :max_price, :min_people, :min_price,
  :payment_collection_date, :pioneer_discount, :title , :published, :website, :image_url

  validates_presence_of :user_id, :payment_collection_date, :image_url

  validates :min_price, :presence => true, :numericality => { :greater_than => 0, :less_than_or_equal_to => :max_price }
  validates :max_price, :presence => true, :numericality => { :less_than_or_equal_to => 100000 } # TODO: review max price

  validates :min_people, :presence => true, :numericality => { :greater_than => 0, :less_than_or_equal_to => :max_people }
  validates :max_people, :presence => true, :numericality => { :less_than_or_equal_to => 100000 } # TODO: review max people

  validate :pcd_in_future


  after_save :add_owner_role #, :check_and_update_statuses
  #before_save :check_and_update_statuses

  before_create :set_default_currency

  scope :published, where(published: true)
  scope :unpublished, where(published: false)
  
  # scope :with_recent_pledges, ->(since = 1.day.ago) { \
  #   Offer.joins("join pledges as offers_pledges on offers.id = offers_pledges.offer_id"). \
  #   where{ (offers_pledges.created_at > since) & (offers_pledges.payment_status >> [1, 2]) & (offers_pledges.user_id!=nil) } \
  # }
  scope :with_recent_pledges, ->(since = 1.day.ago) { Offer.joins("join pledges as offers_pledges on offers.id = offers_pledges.offer_id") \
    .where("offers_pledges.created_at > :then and offers_pledges.payment_status in (1, 2) and offers_pledges.user_id is not NULL", then: since) }

  class << self 
    alias_method :digestable, :with_recent_pledges
    alias_method :digestable_offers, :digestable
  end

  def self.payable 
    all.select { |o| o.active? }
  end

  def payable?
    active? && payment_collection_date >= Date.today
  end

  def payable_pledges
    self.pledges.select { |p| p.payable? }
  end

  def available_pledges
    self.pledges.available
  end

  def status
    result  = :pending
    if current_offer_price
      current_min_insiders_count = self.min_insiders_count(self.current_offer_price)
      if current_min_insiders_count
        current_insiders = self.insiders(self.current_offer_price)
        if current_insiders.count >= current_min_insiders_count
            result = :active
        end
      end
    end

    return result
  end

  def active?
    self.status == :active
  end

  def publish
    update_attribute(:published, true)
  end

  def pending?
    self.status == :pending
  end

  def published?
    published == true
  end


  def status_str
    case self.status
    when :pending
      "Pending"
    when :active
      "Active"
    end
  end

  def accepting_cash?
    available_pledges.count < max_people
  end

  def receiving_cash?
    accepting_cash?
  end

  def next_pledge_price
    next_min_pledge_price = pledge_price_for_people(self.pledges.count + 1)

    price_range_to_check = next_min_pledge_price..current_offer_price
    price = self.max_price

    price_range_to_check.each do |price_to_check|
      insiders_length = insiders(price_to_check).length
      min_insiders_count_price_to_check = min_insiders_count(price_to_check)

      if(insiders_length + 1 >= min_insiders_count_price_to_check)
        price = price_to_check
        break
      end
    end

    price
  end

  def pledge_price_for_people people
     # [[p1, pr1], [p2, pr2], ...]
    if people <= self.min_people
      return self.max_price
    elsif people >= self.max_people
      return self.min_price
    end
    price_steps.reverse.each do |pair|
        #pair[0]: people
        price = pair[1]
        if pair[0] == people
          return price
        end
      end
      return nil
  end

  def current_price
    current_offer_price
  end

  def _current_offer_price
    current_price = self.max_price

    pledges = self.available_pledges
    pledges.each do |pledge|
      amount = pledge.amount

      if(amount <= current_price)

        group = self.available_pledges.where("amount >= ?", amount)

        group_count = group.count

        min_insiders_count = min_insiders_count(amount)

        if(min_insiders_count and group_count >= min_insiders_count)
          current_price = amount
        end
      end
    end

    return current_price
  end

  def current_offer_price
    #normalize prices
    prices = self.prices.reverse.map {|price| price[0]}

    current_price = self.max_price

    #find out current price
    prices.each do |price|

      if insiders(price).length >= min_insiders_count(price)
        current_price = price
        break
      end
    end

    return current_price
  end

  def price_steps
  	price_step = calc_price_step

  	range = (self.min_people..self.max_people)
  	price = self.max_price
  	data = []
  	range.each do |people|
  		data << [people, price.to_i]
      price -= price_step
  	end
    data
  end

  def prices
    price_step = calc_price_step
    range = (self.min_people..self.max_people)
    price = self.max_price
    data = []
    range.each do |prices|
      data << [price.to_i]  # TODO refactor
      price -= price_step
    end
    data
  end

  # def dropdown_prices
  #   price_step = calc_price_step
  #   range = (self.min_people..self.max_people)
  #   price = self.max_price
  #   data = []
  #   range.each do |prices|
  #     data << [price.to_i]  # TODO refactor
  #     price -= price_step
  #   end
  #   data
  # end

  def income
    price_steps.map { |people, price| [people, people * price]}
  end

  def income_values
    income.map { |people, income| income }
  end

  def income_max
    income_values.max
  end

  def income_min
    income_values.min
  end


  def check_status
    status = STATUS_PENDING

    self.prices.reverse.each do |price_el|
      pledges_for_price = self.num_pledges_for_price price_el[0]
      pledges_needed = self.min_insiders_count price_el[0]
      if pledges_for_price >= pledges_needed
        status = STATUS_ACTIVE
        break
      end
    end

    return status
  end



  def calculate_current_price
    #normalize prices
    @prices = self.prices.reverse.map {|price| price[0]}

    self.current_price = nil
    #find out current price
    @prices.each do |price|

      if insiders(price).length >= min_insiders_count(price)
        self.current_price = price

        break
      end

    end
  end

  def check_and_update_statuses

    calculate_current_price

    #update offer status
    if self.current_price
      self.status = STATUS_ACTIVE
      self.insiders(self.current_price).update_all(:status => STATUS_ACTIVE)
      self.outsiders(self.current_price).update_all(:status => STATUS_PENDING)
    else
      self.status = STATUS_PENDING
      #update pledge statuses
      self.available_pledges.update_all(:status => STATUS_PENDING)
    end

    #todo: check if new object...
    update_column(:status, self.status)
    #self.update_attribute(:status, self.status)
  end


  def outsiders price
    outsiders = self.available_pledges.where("amount < ?", price)
  end

  def insiders price
    insiders = self.available_pledges.where("amount >= ?", price)
  end

  def num_pledges_for_price price
    self.available_pledges.reduce(0) do |count, pledge|
      pledge.amount >= price ? count+=1 : count
    end
  end

  def min_insiders_count price
    for point in price_steps
      if(point[1] <= price)
        return point[0]
      end
    end
    nil
  end

  def _next_price
    result = self.max_price

    calculate_current_price #195
    if(!current_price)
      return result
    end

    num_current_insiders = insiders(current_price).length #6

    for people in self.max_people..num_current_insiders
      _price = price_by_people(people)
      _insiders = insiders(_price).length
      if(_insiders >= people - 1)
        result = _price #190
        break
      end
    end

    return result #190
  end


  #DEPRECATED
  def self.recent_pledges
    Pledge.recent
  end

  #DEPRECATED
  def recent_pledges
    pledges.recent
  end


  def self.recently_pledged_offer_ids since = 1.day.ago
    digestable.map &:id
  end

  

  def digestable_followers
     followers - pledgers
  end

  def self.digestable_pledges
    Pledge.where(offer_id: recently_pledged_offer_ids) - recent_pledges
  end

  

  private

  def price_by_people num_people
    # [[p1, pr1], [p2, pr2], ...]
    price_steps.reverse.each do |pair|
      people = pair[0]
      price = pair[1]
      if people == num_people
        return price
      end
    end
    return nil
  end

  def pcd_in_future
    !self.payment_collection_date.nil? && self.payment_collection_date > Date.today
  end

  def extremal_values
    max_people >= min_people > 0 and max_price >= min_price > 0
  end

  def calc_price_step
    price_change = self.max_price - self.min_price
    steps = self.max_people - self.min_people
    price_step = price_change * 1.0/ steps
  end

  def add_owner_role
    user = User.find(self.user_id)
    user.add_role :author, self
  end

  def set_default_currency
    self.currency ||= "ILS"
  end

end