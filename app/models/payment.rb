class Payment < ActiveRecord::Base
  belongs_to :pledge
  
  attr_accessible :pledge_id, :amount, :currency, :timestamp, :ack, :correlationId, :build, :paykey, :payment_exec_status

  validates_presence_of :pledge
  validates_presence_of :amount

  def ppa_request
    @ppa_request ||= PaypalAdaptive::Request.new
  end
  def pd force = false
    paypal_payment_details force
  end

  def paypal_payment_details force = false

    fetch_details = -> { ppa_request.payment_details( "requestEnvelope"=>{"errorLanguage"=>"en_US"}, \
    	"payKey"=>self.paykey) }
    if force
      @payment_details = fetch_details.call
    else
      @payment_details ||= fetch_details.call
    end
  end

end