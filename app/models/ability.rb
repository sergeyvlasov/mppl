class Ability
  include CanCan::Ability

  def initialize(user)

    user ||= User.new # guest user (not logged in)

    if user.has_role? :admin
      can :manage, :all
    else

      can :create, Pledge

      can :contact_us, Offer

      can :read, Offer do |offer|
        offer.published? || user.id == offer.user.id
      end 

      cannot :manage, Category

      #can :read, :all #any user can read anything
      
      can :view_offer_price, Offer do |offer|
        is_seller = offer.user.id == user.id #offer owner
        is_buyer =  offer.available_pledges.where(user_id: user.id).any?
        is_seller or is_buyer
      end

     
      #Offer actions
      # TODO (currently not available) can :create, Offer unless user.id.nil?
      can [:destroy, :update], Offer do |offer| 
        offer.user.id  == user.try(:id)        
      end

      if user.new_record?
        cannot :create, Offer
        cannot :create, Following
      else
        can :create, Offer
        can :create, Pledge 
        can :create, Following
      end


      #cannot :publish, Offer unless user.has_role? :admin



      # Update & Delete actions performed on an instance
      # while Create action performed on a class 
      can [:update, :destroy], Pledge do |pledge|
        pledge.user == user
      end

      cannot [:update, :destroy], Pledge do |pledge|
        pledge.user != user
      end

      can :add_follower, Offer do |offer|
        !user.id.nil?
      end

      can :remove_follower, Offer do |offer|
        !user.id.nil? 
      end

      can :my, Pledge do 
        !user.id.nil?
      end

      can :my, Offer do
        !user.id.nil?
      end

      can [:accept_cash, :receive_cash], Offer do |offer|
        user.id == offer.user.id
      end

      can :view_user_details, User do |user_details|
        user_details.email == user.email
      end

    end
  end
end
