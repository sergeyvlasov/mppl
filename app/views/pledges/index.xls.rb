<?xml version="1.0"?>
<Workbook xmlns="urn:schemas-microsoft-com:office:spreadsheet"
  xmlns:o="urn:schemas-microsoft-com:office:office"
  xmlns:x="urn:schemas-microsoft-com:office:excel"
  xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet"
  xmlns:html="http://www.w3.org/TR/REC-html40">
  <Worksheet ss:Name="Sheet1">
    <Table>
      <Row>
        <Cell><Data ss:Type="String">ID</Data></Cell>
        <Cell><Data ss:Type="String">Pledger</Data></Cell>
        <Cell><Data ss:Type="String">Release Date</Data></Cell>
        <Cell><Data ss:Type="String">Amount</Data></Cell>
        <Cell><Data ss:Type="String">Preapproval Key</Data></Cell>
      </Row>
    <% @offer.available_pledges.each do |pledge| %>
      <Row>
        <Cell><Data ss:Type="Number"><%= pledge.id %></Data></Cell>
        <Cell><Data ss:Type="String"><%= pledge.user.name %></Data></Cell>
        <Cell><Data ss:Type="String"><%= pledge.released_on %></Data></Cell>
        <Cell><Data ss:Type="Number"><%= pledge.amount %></Data></Cell>
        <Cell><Data ss:Type="String"><%= pledge.preapproval_key %></Data></Cell>
      </Row>
    <% end %>
    </Table>
  </Worksheet>
</Workbook>