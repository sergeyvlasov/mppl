class NotificationMailer < ActionMailer::Base
  include ActionView::Helpers::TextHelper
  
  default from: "More People Pay Less <notifications@morepeoplepayless.com>"

  def successful_pledge_buyer(pledge)
  	@pledge = pledge
    @buyer = pledge.user
    @offer = pledge.offer
    #TODO: add locale param to ipn_url, return_url, cancel_url
    @url = offer_url(@offer, { locale: "en" })
    mail(:to => @buyer.email, :subject => "Congrats! you successfully pledged for the #{@offer.title}")
    
  end

  def successful_pledge_seller(pledge)
    @pledge = pledge
    @buyer = pledge.user
    @seller = pledge.offer.user
    @offer = pledge.offer
    @url = offer_url(@offer, { locale: "en" })
    @user_url = user_url(@pledge.user, { locale: "en" })
    mail(:to => @seller.email, :subject => "Good News! #{@buyer.name} just pledged for your #{@offer.title}")
  end

  def daily_digest_buyer(offer, pledger)
      @offer = offer
      @user = pledger
      @offer_url = offer_url(@offer, { locale: "en" })
      puts "sending daily digest buyer: offer: ##{offer.id} pledger: ##{pledger.email}"
      mail(:to => @user.email, :subject => "Good News! #{pluralize(@offer.recent_pledges.count, "Person")} pledged for the #{@offer.title} yesterday.")
  end

  def daily_digest_seller(offer)
      @offer = offer
      @seller = offer.user
      @offer_url = offer_url(@offer, { locale: "en" })
      mail(:to => offer.user.email, :subject => "Daily Digest: #{pluralize(@offer.recent_pledges.count, "Person")} pledged for your #{@offer.title} yesterday" )
  end

  def daily_digest_follower(offer, follower)
      @offer = offer
      @user = follower
      @offer_url = offer_url(@offer, { locale: "en" })
      
      mail(:to => @user.email, :subject => "Good News! #{pluralize(@offer.recent_pledges.count, "Person")} pledged for the #{@offer.title} yesterday.")
  end

  def account_creation(user, password)
      @user = user
      @email = @user.email
      @password = password
      mail(:to => @email, :subject => "Congrats! You Have a MPPL Account")
  end

  def widget_contact_us(contact_form, offer)

    @offer = offer
    @seller = offer.user

    @name = contact_form.name
    @email = contact_form.email
    @phone = contact_form.phone
    @body = contact_form.body

    mail(:to => @seller.email, :subject => "Heads Up! Somebody's interested in your Offer")
  end

  def bounced_pledge_seller(pledge)
    @pledge = pledge
    @offer = @pledge.offer
    @seller = @offer.user
    @offer_url = offer_url(@offer, { locale: "en" })
    mail(:to => @seller.email, :subject => "Hey #{@seller.name}, someone almost pledged for your offer...")
  end

end