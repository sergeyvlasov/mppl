module WidgetsHelper
	
	TYPES = {
	 
		blogger: [680, 750],
		# mobile_portrait: [320, 480],
		# mobile_landscape: [480, 320],
		# small_tablet_portrait: [600, 800],
		# small_tablet_landscape:[800, 600],
		# tablet_portrait: [768, 1024],
		# tablet_landscape: [1024, 768],
		# desktop: [1680, 1050]
  }

	def snippet type = :blogger
		dimensions = WidgetsHelper::TYPES[type]
		width = dimensions[0]
		height = dimensions[1]
		render partial: "widgets/snippet", locals: { width: width, height: height }
	end
end
