require 'simpleform_inputs'

module ApplicationHelper
  def days_left(to_date, from_date = Date.today)
	  (to_date - from_date).to_i
  end

  def title(page_title)
    content_for(:title) { page_title }
  end

  def embedded_paypal_button
  	
  end

end
