module OffersHelper

	SHEKEL_SIGN = "&#8362;"

	def percentage offer
		100 * offer.available_pledges.count.to_f/offer.max_people
	end

	def progress_color offer

		color = "orange"
		color = "grey"  if offer.available_pledges.count < offer.min_people
		color = "blue" if offer.status == Offer::STATUS_ACTIVE

		color

	end

	def unit offer

		case offer.currency
		when "ILS"
			SHEKEL_SIGN
		when "NIS"
			SHEKEL_SIGN
		when "USD"
			"$"
		else
			"$"
		end
	end

	#TODO: factor out to model db dependent code 
	def offer_price_options offer
		offer_next_pledge_price = offer.next_pledge_price
		offer.price_steps.select{|people, price| price <= offer_next_pledge_price }.map do |people, price|

			formatted_price = number_to_currency(price, unit: unit(offer), precision: 0)

			["#{formatted_price} - #{people} #{t('offer.show.people')}".html_safe, price]

		end
	end

	def offer_price_range offer
		max_price = number_to_currency(offer.max_price, unit: unit(offer))

    min_price = number_to_currency(offer.min_price, unit: unit(offer))

    "#{max_price}  &rarr; #{min_price}"
	end

  def offer_people_pledged offer
  	"#{offer.available_pledges.count}/#{offer.max_people}"
  end
end
