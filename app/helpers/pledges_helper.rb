module PledgesHelper
	def pledge_upgrade_explanation
		"In order to increase your pledge amount you will need approve the increased amount through paypal. 
		Once you successfully approve the new pledge we will cancel your original pledge."
	end
end
