class WidgetsController < ApplicationController

  before_filter :prepare_data, except: [:faq, :translations]
  
  
  def show
  end

  def preview
    render layout: false
  end

  def embed
    @container_id = params[:container_id]
  end


  
  def details
  end

  def faq
    render layout: false
  end

  def translations
    respond_to do |format|
      format.js 
    end
  end

  def snippet width=nil, height=nil
    if width
      @viewport[:width] = width
    end
    if height
      @viewport[:height] = height
    end

    render partial: "snippet", layout: false, content_type: 'text/plain'
  end

  def snippet_mobile_portrait #320x480
    snippet(320, 480)
  end
  def snippet_mobile_landscape #480x320
    snippet(480, 320)
  end
  def snippet_small_tablet_portrait #600x800
    snippet(600, 800)
  end
  def snippet_small_tablet_landscape #800x600
    snippet(800, 600)
  end
  def snippet_tablet_portrait #768x1024
    snippet(768, 1024)
  end
  def snipppet_tablet_landscape #(1024x768)
    snippet(1024, 768)
  end

  private 
  def prepare_data
    offer_id = params[:id]
    if(params[:locale])
      @locale = params[:locale]
    else
      @locale = "en"
    end

    @offer = Offer.find(offer_id)
    @pledge = Pledge.new
    @contact_form = ContactForm.new
    
    @price_values = @offer.price_steps
    @income = can?(:manage, :all) ? @offer.income : []
    @max_income = @offer.income_max
    @min_income = @offer.income_min

    @viewport = {
      width: (params[:width] ? params[:width] : 480),
      height: (params[:height] ? params[:height] : 720)
    }

  end
end
