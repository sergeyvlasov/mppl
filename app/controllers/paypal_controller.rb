class PaypalController < ApplicationController

	def pledge
    pledge = params[:pledge]
		offer_id = params[:offer_id]

		@offer = Offer.find offer_id

		@pledge = @offer.pledges.new(offer_id: offer_id, amount: params[:amount])

    @pledge.user_id = current_user.try(:id)

    if @pledge.save
    
      amount = @pledge.amount
      end_date = @offer.payment_collection_date
      owner = current_user
      cancel_url = offer_url(@offer)
      
      ipn_url = "#{Rails.application.config.ipn_host}#{payment_notifications_path}"
      
      data = {
        "custom" => "#{@offer.id}",
        "returnUrl" => thankyou_offer_url(@offer),
        "startingDate" => Date.today.to_s,
        "endingDate" => @offer.payment_collection_date.to_s,
        "displayMaxTotalAmount" => "TRUE",
        "maxTotalAmountOfAllPayments" => amount,
        "requestEnvelope" => {"errorLanguage" => "en_US"},
        "currencyCode" => "ILS",
        "cancelUrl" => cancel_url,
        "ipnNotificationUrl" => ipn_url,
        "memo" => "You are about to pledge a max payment of #{amount} Shekels for the #{@offer.title}. Your pledge will be charged, assuming the offer is ACTIVE on the payment collection date AND your pledge amount is at least equal to the final price, If the final price is lower than what you pledged you will be charged less than the price you have pledged. Good Luck!",
        "maxNumberOfPayments" => "12",
        "maxAmountPerPayment" => amount }

      pay_request = PaypalAdaptive::Request.new
      
      @pay_response = pay_request.preapproval(data)
      
      puts pay_response: @pay_response

      if @pay_response.success?

        preapproval_key = @pay_response["preapprovalKey"]

        @pledge.update_attributes(preapproval_key: preapproval_key)

        redirect_to paypal_url(preapproval_key) #@pay_response.preapproval_paypal_payment_url
      else
        @pledge.destroy
        Rails.logger.error("Preapproval Key error: #{@pay_response.inspect}")
        render action: "new", notice: "Something went wrong. Please contact support."
      end
    else
      render action: "new", notice: "Something went wrong. Please contact support."
    end
	end
  
 

  private 
  def paypal_url preapproval_key
  	"https://www.sandbox.paypal.com/webapps/adaptivepayment/flow/preapproval?preapprovalkey=#{preapproval_key}"
		# "https://www.sandbox.paypal.com/webapps/checkout/webflow/sparta/expresscheckoutvalidatedataflow?execution=e1s2"
	end
end
