class PledgesController < ApplicationController

  before_filter  :authenticate_user!, except: [:create, :preapproval_return, :preapproval_cancel, :paypal_details] 
  load_and_authorize_resource except: [:index, :show, :create, :preapproval_return, :preapproval_cancel, :paypal_details]

  before_filter :get_offer, except: :my
  
  # GET /pledges
  # GET /pledges.json
  def index
    @pledges = @offer.available_pledges
    @pledges_sum = @pledges.sum('amount')

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @pledges }
      format.csv { send_data @pledges.to_csv }
      format.xls # { send_data @pledges.to_csv(col_sep: "\t") }
    end
  end

  def my
    @pledges = current_user.pledges.available
    render action: :my_pledges
  end

  def all
    @pledges = Pledge.all
  end

  # GET /pledges/1
  # GET /pledges/1.json
  def show
    @pledge = @offer.pledges.find(params[:id])
    @pyapal_details = @pledge.pd
    @payment = Payment.new

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @pledge }
    end
  end

  # GET /pledges/new
  # GET /pledges/new.json
  def new
    #delete_previous_pledge_if_exists(params)
    @pledge = @offer.pledges.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @pledge }
    end
  end

  # GET /pledges/1/edit
  def edit
    @pledge = Pledge.find(params[:id])
  end

  # POST /pledges
  def create
    
    @pledge = @offer.pledges.new(params[:pledge])

    @pledge.user_id = current_user.try(:id)

    if @pledge.save
      pay_response = request_preapproval @pledge

      if pay_response.success?
        ppak = pay_response["preapprovalKey"]
        @pledge.update_attributes(preapproval_key: ppak)
        
        redirect_url = paypal_url(pay_response)
        
        respond_to do |format|
          format.html { redirect_to redirect_url }
          format.json { render inline: { url: redirect_url }.to_json }
        end
      else
        @pledge.destroy
        Rails.logger.error("Preapproval Key error: #{pay_response.inspect}")
        render action: "new", notice: "Something went wrong. Please contact support."
      end
    
    else
      render action: "new", notice: "Something went wrong. Please contact support."
    end

  end

  # PUT /pledges/1
  # PUT /pledges/1.json
  def update
    
    @pledge = @offer.pledges.find(params[:id])

    if(@pledge.payment_status == :cash)
      @pledge.update_attributes(params[:pledge])
      redirect_to offer_pledge_path(@offer, @pledge), notice: 'cash pledge updated' and return
    end

    if(@pledge.payment_status != :pledged)
      redirect_to offer_pledge_path(@offer, @pledge), notice: 'Cannot update this pledge' and return
    end



    if params[:pledge][:amount].to_i <= @pledge.preapproved_amount
      _downgrade()
    else
      _upgrade()
    end
    
  end

  def _downgrade
    if @pledge.update_attributes(params[:pledge])
      redirect_to offer_pledge_path(@offer, @pledge), notice: 'Pledge was successfully updated.' 
    else
      render action: "edit", notice: 'Could not update your pledge.' 
    end
  end

  def _upgrade
    #request_preapproval new pledge with ipn_url?cancel_pledge_id=@pledge.id
    new_pledge = @offer.pledges.new params[:pledge]
    new_pledge.user = current_user
    
    if new_pledge.save
      #binding.pry
      pay_response = request_preapproval new_pledge, old_pledge_id: params[:id]
      #binding.pry
    else
      render action: "edit", warning: "Could not modify your pledge, try once more"
    end

    if pay_response.success?
      new_pledge.update_attributes(preapproval_key: pay_response["preapprovalKey"])
      redirect_to paypal_url(pay_response)
    else
      render action: "edit", warning: "Could not preapprove your payment with PayPal, try another amount"
    end
  end

  # DELETE /pledges/1
  # DELETE /pledges/1.json
  def destroy
    @pledge = @offer.pledges.find(params[:id])
    cancelled = @pledge.cancel
    
      respond_to do |format|
        format.html do 
          
          redirect_to offer_pledges_path(@offer), notice: if cancelled
                                                            'Pledge was Cancelled' 
                                                          else
                                                            'Pledge Cancellation Failed'
                                                          end
        end
        format.json { head :no_content }
        format.js do 
          if cancelled
            render inline: "$('#tr_#{@pledge.id}').fadeOut('slow');" 
          else
            render inline: "alert('could not cancel your pledge!');" 
          end
          #render inline: "alert('deleted');" 
        end
      end
  end

  def preapproval_return

    upgraded = false

    pledge = Pledge.find params[:id]
    
    old_pledge = nil      
    if old_pledge_id = params[:old_pledge_id]
      old_pledge = Pledge.find old_pledge_id
    end
    
    details = pledge.paypal_preapproval_details
    
    sender_email = details["senderEmail"]
    pledge.activate old_pledge, sender_email

    upgraded = old_pledge.try(:canceled?) && pledge.try(:pledged?)
    #binding.pry
    if old_pledge_id 
      if upgraded
        redirect_to thankyou_offer_url(@offer), notice: t('helpers.alert.upgraded_pledge',
          default: "You have successfully upgraded your pledge")
      else  
        redirect_to edit_offer_pledge_url[@offer, old_pledge], notice: 'Your Pledge Upgrade Failed' 
      end
    
    else

      if pledge.try(:pledged?)                  
        redirect_to thankyou_offer_url(@offer), notice: t('helpers.alert.successful_pledge', 
          :default => 'You have successfully pledged')
      else
        redirect_to offer_url(@offer), notice: 'Your Pledge Failed'
      end
    
    end
  end

  def preapproval_cancel
    pledge = Pledge.find params[:id]
    pledge.cancel
    redirect_to offer_pledges_path(@offer), notice: if params[:old_pledge_id]
                                                      'Your Pledge Upgrade Canceled' 
                                                    else
                                                      'Your Pledge was Canceled'
                                                    end 
  end

  def paypal_details
    pledge = Pledge.find params[:id]
    details = pledge.paypal_preapproval_details
    render json: details
  end

  private

  def get_offer
    offer_id = params[:offer_id] || Pledge.find(params[:id]).try(:offer_id)
    
    @offer = Offer.find(offer_id) if offer_id
  end

  def request_preapproval pledge, options = {}
    end_date = pledge.offer.payment_collection_date + 1.month
    owner = current_user
    id = pledge.id
      
    if options[:old_pledge_id] #upgrade
      old_pledge_id = options[:old_pledge_id]
      return_url = preapproval_return_url(id, old_pledge_id)
      cancel_url = preapproval_cancel_url(id, old_pledge_id)
    
    else # no upgrade
      old_pledge_suffix = ""
      return_url = preapproval_return_url id
      cancel_url = preapproval_cancel_url id
    end

    ipn_url = "#{Rails.application.config.ipn_host}#{payment_notifications_path}#{old_pledge_suffix}"
    amount = pledge.amount
    data = {
      "returnUrl" => return_url,
      "cancelUrl" => cancel_url,
      "ipnNotificationUrl" => ipn_url,

      "custom" => "#{@offer.id}",
      "startingDate" => Date.today.to_s,
      "endingDate" => @offer.payment_collection_date + 2.months,
      "displayMaxTotalAmount" => "TRUE",
      "maxTotalAmountOfAllPayments" => amount,
      "requestEnvelope" => {"errorLanguage" => "en_US"},
      "currencyCode" => "ILS",
      "memo" => t('paypal.preapproval.memo', amount: amount, offer_title: @offer.title ),
      "maxNumberOfPayments" => "12",
      "maxAmountPerPayment" => amount }

    pay_request = PaypalAdaptive::Request.new
    pay_response = pay_request.preapproval(data)

  end
 


  def paypal_url pay_response, embedded = false
    
    if embedded 
      embedded_paypal_url pay_response["preapprovalKey"]
    else
      pay_response.preapproval_paypal_payment_url 
    end


  end

  def embedded_paypal_url preapproval_key
    
    paypal_endpoint = "https://www.sandbox.paypal.com/webapps/adaptivepayment/flow/preapproval"
    if Rails.env == "production"
      paypal_endpoint = "https://www.paypal.com/webapps/adaptivepayment/flow/preapproval"
    end
   
    "#{paypal_endpoint}?preapprovalkey=#{preapproval_key}"
  end


  def pay
    pledge = Pledge.find(params[:id])
    amount = pledge_params[:pledge][:amount]
    
    #collect the payment
    @result = pledge.collect_payment amount
    respond_to do |format|
      format.json { render json: @result }
    end

  end

end
