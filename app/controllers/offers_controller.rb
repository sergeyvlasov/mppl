class OffersController < ApplicationController

  protect_from_forgery :except => [:widget, :preview, :snippet, :embed, :contact_us]
  before_filter  :authenticate_user!, except: [:index, :thankyou, :widget, :preview, :embed, :snippet, :show, :chart, :contact_us]
  load_and_authorize_resource except: [:thankyou, :widget,  :preview, :snippet, :embed, :contact_us, :chart]

  # GET /offers
  # GET /offers.json
  def index
    @offers = Offer.published
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @offers }
    end
  end

  def my
    @offers = current_user.offers
    render action: :index
  end

  def thankyou
    @offer = Offer.find(params[:id])
  end

  # GET /offers/1
  # GET /offers/1.json
  def show
    offer_id = params[:id]
    user_id = current_user.try(:id)

    @offer = Offer.find(offer_id)
    @pledge = Pledge.new
    @following = Following.find_by_offer_and_user @offer, current_user
    @title = @offer.title
    # @category = @offer.category.name.to_s

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @offer }
    end
  end

  

  def chart
    if params[:id]
      @offer = Offer.find(params[:id]) 
    else
      @offer = Offer.new
    end
    
    @offer.attributes = params[:offer]

    render template: "offers/chart_data.json", content_type: "application/json"
  end


  def preview
   widget
  end

  def snippet
    prepare_data
    render layout: false, content_type: 'text/plain'
  end

  def embed
    prepare_data
  end

  def widget
    prepare_data
    layout :widgets
  end


  def contact_us
    @contact_form = ContactForm.new params[:contact_form]
    @offer = Offer.find(params[:id])

    NotificationMailer.widget_contact_us(@contact_form, @offer).deliver

    respond_to do |format|
      format.json { render json: { success: true } }
      format.js { render nothing: true }
    end

  end


  # GET /offers/new
  # GET /offers/new.json
  def new
    @offer = Offer.new

    @offer.max_price = 3500
    @offer.min_price = 2500
    @offer.max_people = 20
    @offer.min_people = 10

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @offer }
    end
  end

  # GET /offers/1/edit
  def edit
    @offer = Offer.find(params[:id])
  end

  # POST /offers
  # POST /offers.json
  def create
    @offer = Offer.new(params[:offer])
    @offer.user_id = current_user.id

    respond_to do |format|
      if @offer.save
        format.html { redirect_to @offer, notice: 'Offer was successfully created.' }
        format.json { render json: @offer, status: :created, location: @offer }
      else
        format.html { render action: "new" }
        format.json { render json: @offer.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /offers/1
  # PUT /offers/1.json
  def update
    @offer = Offer.find(params[:id])
    if params[:commit] == 'Follow'
      @offer.followers << current_user
    end
    if params[:commit] == 'Publish'
      @offer.published = true
      if(@offer.save)
        redirect_to :back, notice: "Successfully published!"
        return
      end
    end

    respond_to do |format|
      if @offer.update_attributes(params[:offer])
        format.html { redirect_to @offer, notice: 'Offer was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @offer.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /offers/1
  # DELETE /offers/1.json
  def destroy
    @offer = Offer.find(params[:id])
    @offer.destroy

    respond_to do |format|
      format.html { redirect_to offers_url }
      format.json { head :no_content }
    end
  end


  #Offer owner only allowed
  #New action
  def accept_cash

    @offer = Offer.find(params[:id])

    if(@offer.accepting_cash?)

      @pledge = Pledge.new

      #display accept-cash form
      # form action => POST receive_cash
      #the form should include:
      #cash amount
      #buyer details(email!, name, etc.)

    else
      #redirect to @offer#show with notice/warning

      redirect_to @offer, notice: "Can't accept cash " + (@offer.available_pledges.count == @offer.max_people ? "any more" : "yet")
    end
  end

  #Offer owner only allowed
  #Create Action
  def receive_cash
    
    @offer = Offer.find(params[:id])
    @pledge = @offer.pledges.new(params[:pledge])
    @pledge.payment_status = :cash

    if(!@offer.receiving_cash?)
      redirect_to @offer, notice: "Can't accept cash " + (@offer.available_pledges.count == @offer.max_people ? "any more" : "yet")
    else
      #check email
      email = params[:user][:email]
      buyer = User.find_by_email email

      if(!buyer)
        password = KeePass::Password.generate('h{8}')
        name = params[:user][:name]
        begin
        buyer = User.create!(name: name, email: email, password: password, password_confirmation: password)
        rescue
          Rails.logger.error("failed to create a user: #{params.inspect}")
          render nothing: true
          return   
        end
        @pledge.user = buyer
        pledge = @pledge
        NotificationMailer.account_creation(buyer, password).deliver
      end

      @pledge.user = buyer
      pledge = @pledge
      if !@pledge.save
        #redirect_to :back, alert: pledge.errors.full_messages.join("<br>")
        render action: :accept_cash
      else
        redirect_to offer_pledges_path @offer
        NotificationMailer.successful_pledge_buyer(pledge).deliver
      end #if 2
    end #if 1

  end #def



  def add_follower

    @offer = Offer.find(params[:id])
    current_user.try(:follow, @offer)
    @following = Following.find_by_offer_and_user(@offer, current_user)

    respond_to do |format|
      format.html { redirect_to @offer, notice: 'You are now following this offer' }
      format.js
    end
  end

  def remove_follower

    offer = Offer.find params[:id]

    user = current_user

    user.unfollow offer

    respond_to do |format|
      format.html { redirect_to @offer, notice: 'You are no longer following this offer' }
      format.js
    end

  end

  private 
  def prepare_data
    offer_id = params[:id]
    if(params[:locale])
      @locale = params[:locale]
    else
      @locale = "en"
    end

    @offer = Offer.find(offer_id)
    @pledge = Pledge.new
    @contact_form = ContactForm.new
    @viewport = {
      width: 480,
      height: 960
    }
    @price_values = @offer.price_steps
    @income = can?(:manage, :all) ? @offer.income : []
    @max_income = @offer.income_max
    @min_income = @offer.income_min
  end


end
