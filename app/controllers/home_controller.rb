class HomeController < ApplicationController
  def index
   	@offers = Offer.published.limit(20)
   	#TODO:
   	#active offers
   	#featured offers
   	#fresh offers
   	#hot offers
   	#popular offers
   	#most interesting/shared/discussed
   	#CATEGORIES: concerts, courses, food, tech etc.
  end


   # IMPORTANT: the parent_id of main categories must be setted to 0 or nil.
  def explore
  	@offers = Offer.published.limit(20)
    
    categories = Category.all


    @first_level = []
    categories.each do |cat|
      if cat[:parent_id] == 0 || cat[:parent_id] == nil  #hash
        @first_level.push(id: cat[:id], name: cat[:name], subcategories: [] );
      end
    end


    categories.each do |cat|
      if cat[:parent_id] != 0 &&  cat[:parent_id] != nil
        @first_level.each do |fl|
          if fl[:id] == cat[:parent_id]
            fl[:subcategories].push(id: cat[:id], name: cat[:name])
          end
        end
      end
    end

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @first_level }
    end
  end

  def how_it_works
  	@offer = Offer.new(max_price: 2500, min_price: 1000, max_people: 20, min_people: 6)
  end
end
