class ApplicationController < ActionController::Base

  before_filter :set_locale

  protect_from_forgery
  rescue_from CanCan::AccessDenied do |exception|
    redirect_to root_path, :alert => exception.message
  end


  def set_locale
   @locale = I18n.locale = params[:locale] || I18n.default_locale
   if @locale == "he"
    @textalign = "right"
    @dir = "rtl"
    else
      @textalign = "left"
      @dir = "ltr"
    end
  end

  def default_url_options(options={})
    logger.debug "default_url_options is passed options: #{options.inspect}\n"
    { :locale => I18n.locale }
  end

  # unless Rails.application.config.consider_all_requests_local
  #   rescue_from ActionController::RoutingError, with: :render_404
  #   rescue_from ActionController::UnknownController, with: :render_404
  #   rescue_from AbstractController::ActionNotFound, with: :render_404
  #   rescue_from ActiveRecord::RecordNotFound, with: :render_404
  #   rescue_from Exception, with: :render_500
  # end

  private
  def render_404(exception)
    @not_found_path = exception.message
    respond_to do |format|
      format.html { render file: "#{Rails.root}/public/404.html", layout: 'layouts/application', status: 404 }
      format.all { render nothing: true, status: 404 }
    end
  end

  def render_500(exception)
    @error = exception
    respond_to do |format|
      format.html { render template: "#{Rails.root}/public/500.html", layout: 'layouts/application', status: 500 }
      format.all { render nothing: true, status: 500}
    end
  end
end


