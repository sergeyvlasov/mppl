class PaymentNotificationsController < ApplicationController

  protect_from_forgery :except => [:create]
  load_and_authorize_resource except: :create

  ACTIVE = "ACTIVE"
  CANCELED = "CANCELED"


  def index
    @payment_notifications = PaymentNotification.all
  end

  def show
    @payment_notification = PaymentNotification.find(params[:id])
  end

  def create
    pak = params[:preapproval_key]

    ipn = PaymentNotification.create!({
                                params: params.inspect,
                                preapproval_key: pak,
                                approved: params[:approved],
                                status: params[:status],
                                sender_email: params[:sender_email]
                                })
    client = PaypalClient.new
    verified = true #TODO: client.verify_ipn(params)
    if(!verified)
      Rails.logger.error("IPN verification failed: #{params.inspect}")
      render nothing: true
      return
    end

    if(ipn.sender_email.nil? || ipn.sender_email.empty?)
      Rails.logger.error("sender email is empty: #{params.inspect}")
      render nothing: true
      return
    end

    pledge = Pledge.find_by_preapproval_key(pak)
    if(!pledge)
      Rails.logger.error("Pledge for PAK #{pak} not found: #{params.inspect}")
      render nothing: true
      return
    end

    sender_email = ipn.sender_email
    old_pledge = Pledge.find old_pledge_id if params[:old_pledge_id]

    if(ipn.status == ACTIVE)
#      old_pledge.activate , sender_email
      activate_pledge pledge, old_pledge, sender_email
    elsif(ipn.status == CANCELED)
      pledge.cancel(false) unless pledge.canceled?

      if(ipn.approved)
        #TODO
        #NotificationMailer.cancel_pledge_buyer(pledge).deliver
      else
        #TODO
        #send admin notification
        #NotificationMailer.cancel_before_approved_pledge_buyer(pledge).deliver
      end

    end

    render nothing: true
  end

  private

  def activate_pledge pledge, old_pledge, email
    pledge.activate(old_pledge, email)
  end


  

  
end
