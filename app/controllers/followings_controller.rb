class FollowingsController < ApplicationController

  load_and_authorize_resource

  #before_filter

  def create

    following = params[:following]
    offer_id = following[:offer_id]
    user_id  = following[:user_id]
    #user_id ||= current_user.try(:id)
    
    found = Following.where(offer_id: offer_id, user_id: user_id).first

    @following = found || Following.create(following)
    @offer = Offer.find(following[:offer_id])
    @already_following = false
    
    if(current_user && current_user.try(:follows?, @offer))
      @already_following = true
    end

    respond_to do |format|
      format.html { redirect_to @offer, notice: 'You are now following this offer' }
      format.js
    end
  end

  def destroy
    following = params[:following]
    offer_id = following[:offer_id]
    user_id  = following[:user_id]
    
    Following.where(offer_id: offer_id, user_id: user_id).destroy_all
    respond_to do |format|
      format.html { redirect_to @offer, notice: 'You are no longer following this offer' }
      format.js 
    end
  end

  def update
    create
  end
end