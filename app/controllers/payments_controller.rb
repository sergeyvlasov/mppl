class PaymentsController < ApplicationController
  protect_from_forgery :except => [:create]

  before_filter :get_offer, except: :all
  
  def all
    @payments = Payment.all
  end


  # GET /payments
  # GET /payments.json
  def index
    @payments = @offer.payments.all
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @payments }
    end
  end

  def new
    @payment = @offer.payments.new
    @pledgers = @offer.pledges
      respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @pledge }
    end
  end

  # GET /payments/1
  # GET /payments/1.json
  def show
    @payment = Payment.find(params[:id])
    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @payment }
    end
  end

  # POST /payments
  # POST /payments.json
  def _create
    #parse parameters
    #pledge_id, amount
    #collect the payment
    ppac = PPAClient.new
    @payment = ppac.collect_pledge_payment pledge, amount
    respond_to do |format|
      format.json { render json: @payment, status: :created, location: @payment }
    end
  end

  def create
    @payment = @offer.payments.new(params[:payment])
    if respond_to do |format|
        if @payment.save
          format.html { redirect_to @offer, notice: 'Payment was successfully created.' }
          format.json { render json: @offer, status: :created, location: @offer }
        else
          format.html { render action: "new" }
        end
      end
    else
      Rails.logger.error("***************PAYMENT #{params.inspect}")
      Payment.create!(:params => params, :ack => params[:ack], :paykey => params[:paykey], :payment_exec_status => params[:payment_exec_status])

      render :nothing => true
    end
  end

#   def edit
#     flash notice: "This action is not available!"
#     redirect_to root_path
#   end

#   # PUT /payments/1
#   # PUT /payments/1.json
#   def update
#     @payment = Payment.find(params[:id])

#     respond_to do |format|
#       if @payment.update_attributes(params[:payment])
#         format.html { redirect_to @payment, notice: 'Payment was successfully updated.' }
#         format.json { head :no_content }
#       else
#         format.html { render action: "edit" }
#         format.json { render json: @payment.errors, status: :unprocessable_entity }
#       end
#     end
#   end

#   # DELETE /payments/1
#   # DELETE /payments/1.json
#   def destroy
#     @payment = Payment.find(params[:id])
#     @payment.destroy

#     respond_to do |format|
#       format.html { redirect_to payments_url }
#       format.json { head :no_content }
#     end
#   end

end

  private

  def get_offer
    @offer = Offer.find(params[:offer_id])
  end