$(function(){

    var $container = $('#offer_container');
    $container.imagesLoaded(function(){
      $container.masonry({

        itemSelector : '.box',
        gutterWidth: 20,
        isFitWidth: true,
        isAnimated: !Modernizr.csstransitions,
        animationOptions: {
          duration: 200,
          easing: 'linear',
          queue: false
        }
      });
    });

    $('.offer').mouseenter(function(){
      var info = $(this).children(".info").first();
      info.fadeIn(1).mouseleave(function(){
        $(this).fadeOut(1);
      });
    });

    $('.offer').click(function(){
     window.location=$(this).find("a").attr("href");
     return false;
});

  });