var mppl_chart = function(){
  var chart, highcharts_chart;

  chart = {};

  highcharts_chart = {};

  $(function(){

    var income_axis, income_series;

    if (typeof price_values !== "undefined" && price_values !== null) {
      
      highcharts_chart = {
        chart: {
          renderTo: "mppl_chart",
          type: "areaspline"
        },
        title: {
          text: translations.title
        },
        legend: {
          enabled: false
        },
        xAxis: {
          title: {
            text: translations.people
          },
          allowDecimals: false
        },
        yAxis: [
          {
            plotLines: [
              {
                color: "#FFFF00",
                width: 2,
                value: 0
              }
            ],
            title: {
              text: translations.price
            }
          }
        ],
        tooltip: {
          formatter: function() {
            return "<b>" + translations.people + "</b> " + ': ' + this.x + "<br><b>" + translations.price + "</b>" + ': ' + this.y;
          }
        },
        plotOptions: {
          areaspline: {
            fillOpacity: 0.3,
          }
        },
        series: [
          {
            id: "price",
            name: translations.price,
            data: price_values,
            type: "spline"
          }
        ]
      };


      if (income.length > 0) {
        income_axis = {
          //min: min_income,
          //max: max_income,
          title: {
            text: translations.income
          },
          opposite: true,
          plotLines: [
            {
              color: "#FF0000",
              width: 2,
              value: 0
            }
          ]
        };
        income_series = {
          id: "income",
          name: translations.income,
          data: income,
          yAxis: 1
        };
        highcharts_chart.yAxis.push(income_axis);
        highcharts_chart.series.push(income_series);
      }

    }
    if ((typeof Highcharts !== "undefined" && Highcharts !== null) && (typeof price_values !== "undefined" && price_values !== null)) {
      chart = new Highcharts.Chart(highcharts_chart);
      //make chart object accessible from other code
      $.mppl_chart = chart;
      return chart;
    }
  });

  return highcharts_chart;

};
