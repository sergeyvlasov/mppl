create a preapprovalKey

curl -s --insecure -H "X-PAYPAL-SECURITY-USERID: elanpe_1335106442_biz_api1.gmail.com" -H "X-PAYPAL-SECURITY-PASSWORD: 1335106467" -H "X-PAYPAL-SECURITY-SIGNATURE: AX2COqhvCZCIs.ISIwNAOF3MzORuA6j8GfE-H0Aej6LCg8J6.niPg92r" -H "X-PAYPAL-REQUEST-DATA-FORMAT: NV" -H "X-PAYPAL-RESPONSE-DATA-FORMAT: NV" -H "X-PAYPAL-APPLICATION-ID: APP-80W284485P519543T" https://svcs.sandbox.paypal.com/AdaptivePayments/Preapproval  -d  "cancelUrl=http://mppl.herokuapp.com/cancelurl.html&currencyCode=USD&endingDate=2012-08-13T08%3A00%3A00.000Z&maxTotalAmountOfAllPayments=15.00&requestEnvelope.errorLanguage=en_US&returnUrl=http://mppl.herokuapp.com/returnurl.html&startingDate=2012-05-15Z&memo=create-preapproval-key"

responseEnvelope.timestamp=2012-05-15T06%3A24%3A00.115-07%3A00&responseEnvelope.ack=Success&responseEnvelope.correlationId=4413eb2eb9c0f&responseEnvelope.build=2915738&preapprovalKey=PA-8DD4722709482513H




Approving a Preapproval

https://sandbox.paypal.com/webscr?cmd=_ap-preapproval&preapprovalkey=PA-8DD4722709482513H




Collecting the money of a preapproval

curl -s --insecure -H "X-PAYPAL-SECURITY-USERID: elanpe_1335106442_biz_api1.gmail.com" -H "X-PAYPAL-SECURITY-PASSWORD: 1335106467" -H "X-PAYPAL-SECURITY-SIGNATURE: AX2COqhvCZCIs.ISIwNAOF3MzORuA6j8GfE-H0Aej6LCg8J6.niPg92r" -H "X-PAYPAL-REQUEST-DATA-FORMAT: NV" -H "X-PAYPAL-RESPONSE-DATA-FORMAT: NV" -H "X-PAYPAL-APPLICATION-ID: APP-80W284485P519543T" https://svcs.sandbox.paypal.com/AdaptivePayments/Pay  -d  "cancelUrl=http://mppl.herokuapp.com/cancelurl.html&currencyCode=USD&amount=15.00&requestEnvelope.errorLanguage=en_US&returnUrl=http://mppl.herokuapp.com/returnurl.html&preapprovalKey=PA-8DD4722709482513H&memo=pay-with-preapproval-key&actionType=PAY&receiverList.receiver(0).amount=15&receiverList.receiver(0).email=elanpe_1335106442_biz@gmail.com"

responseEnvelope.timestamp=2012-05-15T06%3A36%3A06.242-07%3A00&responseEnvelope.ack=Success&responseEnvelope.correlationId=5db500426986b&responseEnvelope.build=2915738&payKey=AP-5JA23321SR266044R&paymentExecStatus=COMPLETED





Preapproval details check

curl -s --insecure -H "X-PAYPAL-SECURITY-USERID: elanpe_1335106442_biz_api1.gmail.com" -H "X-PAYPAL-SECURITY-PASSWORD: 1335106467" -H "X-PAYPAL-SECURITY-SIGNATURE: AX2COqhvCZCIs.ISIwNAOF3MzORuA6j8GfE-H0Aej6LCg8J6.niPg92r" -H "X-PAYPAL-REQUEST-DATA-FORMAT: NV" -H "X-PAYPAL-RESPONSE-DATA-FORMAT: NV" -H "X-PAYPAL-APPLICATION-ID: APP-80W284485P519543T" https://svcs.sandbox.paypal.com/AdaptivePayments/PreapprovalDetails  -d  "&requestEnvelope.errorLanguage=en_US&preapprovalKey=PA-7SK690334B993070R&memo=preapproval-details-check"

responseEnvelope.timestamp=2012-06-05T04%3A53%3A26.092-07%3A00&responseEnvelope.ack=Success&responseEnvelope.correlationId=d68d0b58b29f9&responseEnvelope.build=2945555&approved=true&cancelUrl=http%3A%2F%2Fmppl.herokuapp.com%2Foffers%2F1%2Fpledges%2Fnew%3Fauth%3Dfb8d9314-ea4e-4714-a71d-431712ef3e6d%26pledge_id%3D23&curPayments=0&curPaymentsAmount=0.00&curPeriodAttempts=0&currencyCode=USD&dateOfMonth=0&dayOfWeek=NO_DAY_SPECIFIED&endingDate=2012-08-26T23%3A59%3A11.000-07%3A00&maxAmountPerPayment=27.00&maxNumberOfPayments=1&maxTotalAmountOfAllPayments=27.00&paymentPeriod=NO_PERIOD_SPECIFIED&pinType=NOT_REQUIRED&returnUrl=http%3A%2F%2Fmppl.herokuapp.com%2Foffers%2F1%3Fauth%3Dfb8d9314-ea4e-4714-a71d-431712ef3e6d%26pledge_id%3D23&senderEmail=epbuy_1335110988_per%40gmail.com&memo=Your+are+about+to+pledge+a+max+payment+of+27+dollars+for+the+RoR+Course.+If+your+pledge+is+active+on+the+payment+collection+date+you+will+be+charged+according+to+the+final+price+of+the+offer.+Good+Luck%21&startingDate=2012-06-03T00%3A00%3A11.000-07%3A00&status=ACTIVE&displayMaxTotalAmount=false%              




Canceled Preapproval details check

curl -s --insecure -H "X-PAYPAL-SECURITY-USERID: elanpe_1335106442_biz_api1.gmail.com" -H "X-PAYPAL-SECURITY-PASSWORD: 1335106467" -H "X-PAYPAL-SECURITY-SIGNATURE: AX2COqhvCZCIs.ISIwNAOF3MzORuA6j8GfE-H0Aej6LCg8J6.niPg92r" -H "X-PAYPAL-REQUEST-DATA-FORMAT: NV" -H "X-PAYPAL-RESPONSE-DATA-FORMAT: NV" -H "X-PAYPAL-APPLICATION-ID: APP-80W284485P519543T" https://svcs.sandbox.paypal.com/AdaptivePayments/PreapprovalDetails  -d  "&requestEnvelope.errorLanguage=en_US&preapprovalKey=PA-5LV85415YA982242J&memo=preapproval-details-check"

responseEnvelope.timestamp=2012-06-05T04%3A58%3A14.886-07%3A00&responseEnvelope.ack=Success&responseEnvelope.correlationId=a165e6c175ce2&responseEnvelope.build=2945555&approved=true&cancelUrl=http%3A%2F%2Fmppl.herokuapp.com%2Foffers%2F1%2Fpledges%2Fnew%3Fauth%3D28fadc9b-5843-4039-afa6-36099e46b5f5%26pledge_id%3D4&curPayments=0&curPaymentsAmount=0.00&curPeriodAttempts=0&currencyCode=USD&dateOfMonth=0&dayOfWeek=NO_DAY_SPECIFIED&endingDate=2012-08-25T23%3A59%3A07.000-07%3A00&maxAmountPerPayment=22.00&maxNumberOfPayments=1&maxTotalAmountOfAllPayments=22.00&paymentPeriod=NO_PERIOD_SPECIFIED&pinType=NOT_REQUIRED&returnUrl=http%3A%2F%2Fmppl.herokuapp.com%2Foffers%2F1%3Fauth%3D28fadc9b-5843-4039-afa6-36099e46b5f5%26pledge_id%3D4&senderEmail=epbuy_1335110988_per%40gmail.com&memo=Your+are+about+to+pledge+a+max+payment+of+22+dollars+for+the+RoR+Course.+If+your+pledge+is+active+on+the+payment+collection+date+you+will+be+charged+according+to+the+final+price+of+the+offer.+Good+Luck%21&startingDate=2012-05-29T00%3A00%3A07.000-07%3A00&status=CANCELED&displayMaxTotalAmount=false%   



Bounced Preapproval details check

curl -s --insecure -H "X-PAYPAL-SECURITY-USERID: elanpe_1335106442_biz_api1.gmail.com" -H "X-PAYPAL-SECURITY-PASSWORD: 1335106467" -H "X-PAYPAL-SECURITY-SIGNATURE: AX2COqhvCZCIs.ISIwNAOF3MzORuA6j8GfE-H0Aej6LCg8J6.niPg92r" -H "X-PAYPAL-REQUEST-DATA-FORMAT: NV" -H "X-PAYPAL-RESPONSE-DATA-FORMAT: NV" -H "X-PAYPAL-APPLICATION-ID: APP-80W284485P519543T" https://svcs.sandbox.paypal.com/AdaptivePayments/PreapprovalDetails  -d  "&requestEnvelope.errorLanguage=en_US&preapprovalKey=PA-5KA03419479146834&memo=preapproval-details-check"

responseEnvelope.timestamp=2012-06-05T05%3A03%3A05.363-07%3A00&responseEnvelope.ack=Success&responseEnvelope.correlationId=233409c73c009&responseEnvelope.build=2945555&approved=false&cancelUrl=http%3A%2F%2Fmppl.herokuapp.com%2Foffers%2F4%2Fpledges%2Fnew%3Fauth%3D0bd1a397-7f5f-4dab-b80d-983669f235d8%26pledge_id%3D25&curPayments=0&curPaymentsAmount=0.00&curPeriodAttempts=0&currencyCode=USD&dateOfMonth=0&dayOfWeek=NO_DAY_SPECIFIED&endingDate=2012-06-11T23%3A59%3A43.000-07%3A00&maxAmountPerPayment=59.00&maxNumberOfPayments=1&maxTotalAmountOfAllPayments=59.00&paymentPeriod=NO_PERIOD_SPECIFIED&pinType=NOT_REQUIRED&returnUrl=http%3A%2F%2Fmppl.herokuapp.com%2Foffers%2F4%3Fauth%3D0bd1a397-7f5f-4dab-b80d-983669f235d8%26pledge_id%3D25&memo=Your+are+about+to+pledge+a+max+payment+of+59+dollars+for+the+Art+Course.+If+your+pledge+is+active+on+the+payment+collection+date+you+will+be+charged+according+to+the+final+price+of+the+offer.+Good+Luck%21&startingDate=2012-06-05T00%3A00%3A43.000-07%3A00&status=ACTIVE&displayMaxTotalAmount=false%   




Non exiting preapproval key check

curl -s --insecure -H "X-PAYPAL-SECURITY-USERID: elanpe_1335106442_biz_api1.gmail.com" -H "X-PAYPAL-SECURITY-PASSWORD: 1335106467" -H "X-PAYPAL-SECURITY-SIGNATURE: AX2COqhvCZCIs.ISIwNAOF3MzORuA6j8GfE-H0Aej6LCg8J6.niPg92r" -H "X-PAYPAL-REQUEST-DATA-FORMAT: NV" -H "X-PAYPAL-RESPONSE-DATA-FORMAT: NV" -H "X-PAYPAL-APPLICATION-ID: APP-80W284485P519543T" https://svcs.sandbox.paypal.com/AdaptivePayments/PreapprovalDetails  -d  "&requestEnvelope.errorLanguage=en_US&preapprovalKey=PA-5KA03419472146834&memo=preapproval-details-check"


responseEnvelope.timestamp=2012-06-05T06%3A12%3A06.120-07%3A00&responseEnvelope.ack=Failure&responseEnvelope.correlationId=a8b8a42533ee6&responseEnvelope.build=2945555&error(0).errorId=580022&error(0).domain=PLATFORM&error(0).subdomain=Application&error(0).severity=Error&error(0).category=Application&error(0).message=Invalid+request+parameter%3A+Preapproval+Key+with+value+PA-5KA03419472146834&error(0).parameter(0)=preapprovalId&error(0).parameter(1)=PA-5KA03419472146834%  










curl -s --insecure -H "X-PAYPAL-SECURITY-USERID: elanperach_api1.gmail.com" \
-H "X-PAYPAL-SECURITY-PASSWORD: RWEGQSV5ZN45HSUV" \
-H "X-PAYPAL-SECURITY-SIGNATURE: A6emPSnY6e-Tff6wyFrwZSg4O4FHANxwsAbOhZRqIQyHQ6Xmx3Tssj5u  " \
-H "X-PAYPAL-REQUEST-DATA-FORMAT: NV" \
-H "X-PAYPAL-RESPONSE-DATA-FORMAT: NV" \
-H "X-PAYPAL-APPLICATION-ID: APP-9CA638125N169892W" \
https://svcs.paypal.com/AdaptivePayments/PreapprovalDetails  \
-d  "&requestEnvelope.errorLanguage=en_US&preapprovalKey=PA-8K236884D3189822K&memo=preapproval-details-check"


Collect Payment

curl -s --insecure -H "X-PAYPAL-SECURITY-USERID: elanperach_api1.gmail.com" \
-H "X-PAYPAL-SECURITY-PASSWORD: RWEGQSV5ZN45HSUV" \
-H "X-PAYPAL-SECURITY-SIGNATURE: A6emPSnY6e-Tff6wyFrwZSg4O4FHANxwsAbOhZRqIQyHQ6Xmx3Tssj5u  " \
-H "X-PAYPAL-REQUEST-DATA-FORMAT: NV" \
-H "X-PAYPAL-RESPONSE-DATA-FORMAT: NV" \
-H "X-PAYPAL-APPLICATION-ID: APP-9CA638125N169892W" \
https://svcs.paypal.com/AdaptivePayments/Pay \
-d  "&requestEnvelope.errorLanguage=en_US&preapprovalKey=PA-3DM84162XE479114M&actionType=PAY&senderEmail=dorineperach@gmail.com&receiverList.receiver(0).email=vlasov.family@gmail.com&receiverList.receiver(0).amount=3000&currencyCode=ILS&cancelUrl=http://morepeoplepayless.com&returnUrl=http://morepeoplepayless.com&ipnNotificationUrl=http://morepeoplepayless.com/payment_notifications"