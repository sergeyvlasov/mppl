class PaypalClient 


  def verify_ipn data, options = {}
    if options[:raw]
      ipn_raw = data
    else
      ipn_data = data.clone
      ipn_data.delete :controller
      ipn_data.delete :action
      ipn_data.delete :pledge_id
      ipn_raw = ipn_data.map { |k,v| "#{k}=#{v}" }.join('&')
    end
    verify_ipn_wih_paypal_adaptive ipn_raw
  end

  def verify_ipn_wih_paypal_adaptive data
    ipn = PaypalAdaptive::IpnNotification.new
    ipn.send_back data
    ipn.verified?

  end

  private :verify_ipn_wih_paypal_adaptive
end

#Monkey-patching for PPA IPN
module PaypalAdaptive
  class IpnNotification

    def send_back(data)
      data = "cmd=_notify-validate&#{data}"
      #FIXME!!! add it to config
      #@paypal_base_url = "http://www.sandbox.paypal.com"
      url = URI.parse @paypal_base_url
      http = Net::HTTP.new(url.host, 443)
      http.use_ssl = true
      http.verify_mode = OpenSSL::SSL::VERIFY_NONE #OpenSSL::SSL::VERIFY_PEER

      path = "#{@paypal_base_url}/cgi-bin/webscr"
      response = http.post(path, data)
      response_data = response.body
      
      puts path
      puts data
      puts response_data
      
      @verified = response_data == "VERIFIED"
    end

    def _send_back(data)
      data = "cmd=_notify-validate&#{data}"
      url = URI.parse @paypal_base_url
      http = Net::HTTP.new(url.host, 443)
      http.use_ssl = true
      http.verify_mode = OpenSSL::SSL::VERIFY_PEER
      http.ca_path = @ssl_cert_path unless @ssl_cert_path.nil?

      if @ssl_cert_file
        # cert = File.read(@ssl_cert_file)
        # http.cert = OpenSSL::X509::Certificate.new(cert)
        # http.key = OpenSSL::PKey::RSA.new(cert)
        http.ca_file = @ssl_cert_file
      end

      path = "#{@paypal_base_url}/cgi-bin/webscr"
      response_data = http.post(path, data).body

      @verified = response_data == "VERIFIED"
    end
  end
end