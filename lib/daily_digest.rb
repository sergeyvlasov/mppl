class DailyDigest

	include ActionView::Helpers::TextHelper
	include Rails.application.routes.url_helpers

	attr_reader :day

	def initialize day = Date.today
		@day = day
	end

	
	def self.daily_digest_buyer_receivers
		aged = Pledge.aged.joins(:offer).merge(Offer.with_recent_pledges).map { |p| { offer_id: p.offer_id, user_id: p.user_id } }.uniq
		recent = (Pledge.recent.map { |p| { user_id: p.user_id, offer_id: p.offer_id } }).uniq
		result = aged - recent
		result
	end

	def self.daily_digest_seller_receivers
		Offer.digestable.uniq
	end
	
	def self.daily_digest_buyer
		puts "daily_digest_buyer method begin"
		daily_digest_buyer_receivers.each do |o|
			offer = Offer.find(o[:offer_id])
			pledger = User.find(o[:user_id])
      NotificationMailer.daily_digest_buyer(offer, pledger).deliver
    end
    puts "daily_digest_buyer method end"
	end

	def self.daily_digest_seller
		puts "daily_digest_seller"
		daily_digest_seller_receivers.each do |offer|
			puts "offer: #{offer.title} seller: #{offer.user.email}"
      NotificationMailer.daily_digest_seller(offer).deliver
    end
  end

  def self.daily_digest_follower
  	Offer.digestable.each do |offer|
  		offer.digestable_followers.each do |follower|
        mail_message = NotificationMailer.daily_digest_follower(offer, follower)
        mail_message.deliver
      end
    end
  end

end