class PPAClient

	def collect_payments offer_id

		puts "offer_id: #{offer_id}"

		@offer = Offer.where(id: offer_id).first

		abort "offer #{offer_id} not found" if @offer.nil?

		pledges = @offer.payable_pledges

		abort "nothing to collect!" unless pledges.any?

		#seller_email = Rails.configuration.seller_email
		# mod
		seller_email = @offer.user.paypal
		puts "Seller Email: #{seller_email}"

			base_url = MPPL::Application.config.ipn_host
	   	return_url = "#{base_url}/offers/#{@offer.id}/thankyou"
	   	cancel_url = "#{base_url}/offers/#{@offer.id}"
	   	amount = @offer.current_offer_price
			
			pledges.each do |pledge|
			puts "pledge: #{pledge.inspect}"

	        data = {
	            "returnUrl" => return_url, #"http://morepeoplepayless.com",
	            # mod
	            "amount" => amount, #pledge.amount,
	            "requestEnvelope" => {"errorLanguage" => "en_US"},
	            "currencyCode" => "ILS",
	            "cancelUrl" =>  cancel_url, # "http://mppl.herokuapp.com/cancelurl.html",
	            "receiverList"=>{
	            	"receiver"=>[
	            		{
	            			"email"=>seller_email, 
	            			"amount"=> amount}]},
	            "preapprovalKey" => pledge.preapproval_key,
	            "actionType" => "PAY"
	        }

	        pay_request = PaypalAdaptive::Request.new
	        pay_response = pay_request.pay(data)

	        puts pay_response: pay_response.inspect

	        if pay_response.success?
	            pledge.payment_status = pay_response["paymentExecStatus"]

	            pledge.create_payment!(paykey: pay_response["payKey"], currency: pay_response["currencyCode"],
											                payment_exec_status: pay_response["paymentExecStatus"], 
											                amount: pledge.amount, pledge_id: pledge)
	            puts "payment sucessful"
	        else
	            puts "payment failed: #{pay_response.inspect}"
	        end
		end
	end

	def collect_pledge_payment pledge, amount
   offer = pledge.offer
   if(offer.user.paypal.blank?)
   	puts "seller's paypal should not be blank"
   	return false
   end
   base_url = MPPL::Application.config.ipn_host
   return_url = "#{base_url}/offers/#{offer.id}/thankyou"
   cancel_url = "#{base_url}/offers/#{offer.id}"
   data = {
   		"returnUrl" => return_url, 
   		"cancelUrl" => cancel_url,
      "amount" => amount,
      "requestEnvelope" => {"errorLanguage" => "en_US"},
      "currencyCode" => "ILS",
      "receiverList"=>{
      	"receiver"=>[{"email"=>offer.user.paypal, "amount"=> amount}]},
      "preapprovalKey" => pledge.preapproval_key,
	  "actionType" => "PAY"
			}
		puts data: data
    pay_request = PaypalAdaptive::Request.new
    pay_response = pay_request.pay(data)
    puts pay_response: pay_response.inspect
    if pay_response.success?
        #TODO: pledge.payment_status = pay_response["paymentExecStatus"]
        begin
        payment = pledge.create_payment!(paykey: pay_response["payKey"], currency: pay_response["currencyCode"],
								                payment_exec_status: pay_response["paymentExecStatus"],
								                amount: pledge.amount, pledge_id: pledge)
        result = payment
        puts "payment sucessful"
        rescue Exception => ex
        	puts ex
        	result = ex
        end	

    else
        puts "payment failed: #{pay_response.inspect}"
        result = pay_response
    end
    return result
	end

	def collect_offer_payments offer

		pledges = offer.payable_pledges

		abort "nothing to collect!" unless pledges.any?

		seller_email = offer.user.paypal #Rails.configuration.seller_email #PaypalAdaptive::configs[Rails.env].headers["X-PAYPAL-SECURITY-USERID"]

		puts "Seller Email: #{seller_email}"

		pledges.each do |pledge|
			collect_pledge_payment pledge
		end
	end

	def sync_pledges
		pledges = Pledge.where("payment_status = 0 and preapproval_key is not NULL").all
		pledges.each do |pledge|
			data = {
			  'preapprovalKey' => pledge.preapproval_key,
			  'requestEnvelope' => {
			  	'errorLanguage' => 'en_US'
			  }
			}

			pay_request = PaypalAdaptive::Request.new

			pay_response = pay_request.preapproval_details(data)

			if pay_response["responseEnvelope"] && pay_response["responseEnvelope"]["ack"] == "Success"

				if pay_response["status"] == "ACTIVE" && pay_response["approved"] == "true"

					pledge.update_attribute(:payment_status, :pledged)
					
				elsif pay_response["status"] == "ACTIVE" && pay_response["approved"] == "false"

					#buyer: what happened?
					pledge.cancel
					
					#TODO: not approved: send 3 emails
					#admin: link_to non approved pledge
					#seller: your offer has a non approved pledge

					NotificationMailer.bounced_pledge_seller(pledge).deliver

				elsif pay_response["status"] == "CANCELED" && pay_response["approved"] == "true"

					#TODO: canceled afterwards
					pledge.update_attribute(:payment_status, :canceled)
					
				elsif pay_response["status"] == "CANCELED" && pay_response["approved"] == "false"

					#TODO: canceled right away
					pledge.update_attribute(:payment_status, :canceled)
					
				end
			else
				puts "Communication error..."
				puts pay_response.inspect
				#TODO: send admin email
			end

		end
	end

	
end
