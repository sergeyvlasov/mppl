
  desc "This task synchronizes pledges with PayPal"
  task sync_pledges: :environment do
    ppac = PPAClient.new
      ppac.sync_pledges
      puts "sync_pledges completed: #{Time.now}"
  end

  desc "This task collects PayPal preapproved payments for given offer id"
  task :collect_payments => :environment do |t|

    ppac = PPAClient.new
    offer_id = ENV['offer_id']
    abort "offer_id argument is missing!" if offer_id.nil?
    ppac.collect_payments offer_id
  end

  desc "This is a new task which collects PayPal preapproved payments for given offer id"
  task :collect_offer_payments => :environment do |t|

    ppac = PPAClient.new
    offer_id = ENV['offer_id']
    abort "offer_id argument is missing!" if offer_id.nil?
    offer = Offer.find offer_id
    ppac.collect_offer_payments offer
  end

  desc "This task sends a daily digest email to offer owners"
  task :daily_digest_seller => [:environment, :sync_pledges] do |t|
      DailyDigest.daily_digest_seller
  end

  desc "This task sends a daily digest email to pledgers"
  task :daily_digest_buyer => [:environment, :sync_pledges] do |t|
      puts "task #{t} has begun..."
      DailyDigest.daily_digest_buyer
  end

  desc "This task sends a daily digest email to offer followers"
  task :daily_digest_follower => [:environment, :sync_pledges] do |t|
      DailyDigest.daily_digest_follower
  end

  desc "This task runs all daily digests"
  task :daily_digests => [:sync_pledges, :daily_digest_buyer, :daily_digest_follower, :daily_digest_seller]