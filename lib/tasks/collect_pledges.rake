require 'pp'
desc "charge preapproved payment pledge"
task :collect_pledges => :environment do

	@offer = Offer.find(1)

    # debug @offer.pledges


    @offer.payable_pledges.each do |pledge|

        puts "pledge: #{pledge.inspect}"

        data = {
            "returnUrl" => "http://mppl.herokuapp.com/returnurl.html",
            "amount" => pledge.amount,
            "requestEnvelope" => {"errorLanguage" => "en_US"},
            "currencyCode" => "USD",
            "cancelUrl" => "http://mppl.herokuapp.com/cancelurl.html",
            "receiverList"=>{"receiver"=>[{"email"=>"elanpe_1335106442_biz@gmail.com", "amount"=> pledge.amount}]},
            "preapprovalKey" => pledge.preapproval_key,
            "actionType" => "PAY"
        }

        pay_request = PaypalAdaptive::Request.new
        pay_response = pay_request.pay(data)

        if pay_response.success?
            pledge.payment_status = pay_response["paymentExecStatus"]
            @offer.payments.create!(paykey: pay_response["payKey"], currency: pay_response["currencyCode"],
                payment_exec_status: pay_response["paymentExecStatus"],
                amount: pledge.amount, pledge_id: pledge)
            puts "Payment sucessful"
        else
            puts "payment failed: #{pay_response.inspect}"
        end
    end
end


