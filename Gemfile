require 'rbconfig'
HOST_OS = RbConfig::CONFIG['host_os']
source 'http://rubygems.org'

ruby '1.9.3'
gem 'rails', '3.2.6'

group :assets do
  gem 'sass-rails'
  gem 'coffee-rails'
  gem 'uglifier', '>= 1.0.3'
  gem 'jquery-ui-rails'
  #gem "zurb-foundation"
  if HOST_OS =~ /linux/i
    gem 'therubyracer', '>= 0.9.8'
  end
end

gem 'jquery-rails'
gem "devise"
gem 'humanize' # 5.humanize == 'five'; usable for zurb-fundation's grid classes
gem "simple_form"
gem "cancan", ">= 1.6.7"
gem "rolify", ">= 3.1.0"
gem 'paypal_adaptive'
gem 'paypal-ipn', :require => 'paypal'
gem 'rails-i18n'
gem "gravatar_image_tag"
gem 'localeapp'
gem 'pg'

group :development, :test do
  gem "rspec-rails"
  gem 'sqlite3'
  gem 'debugger'
  gem 'quiet_assets'
  gem "factory_girl_rails"
  gem "faker"
end

group :development do
  gem 'brakeman'
  gem "letter_opener"
  gem 'pry-rails'
  gem 'pry_debug'
  gem 'sextant'
  gem 'ruby_parser'
  gem 'hpricot'
  gem "guard", ">= 0.6.2"
  case HOST_OS
    when /darwin/i
      gem 'rb-fsevent'
      gem 'growl'
    when /linux/i
      gem 'libnotify'
      gem 'rb-inotify'
    when /mswin|windows/i
      gem 'rb-fchange'
      gem 'win32console'
      gem 'rb-notifu'
  end
  gem "guard-bundler", ">= 0.1.3"
  gem "guard-rails", ">= 0.0.3"
  gem "guard-livereload", ">= 0.3.0"
  gem "guard-rspec", ">= 0.4.3"
  gem 'guard-spork'

  gem "guard-cucumber", ">= 0.6.1"
  gem "em-websocket"
  gem "seed_dump"
  gem "nifty-generators"
end

group :test do
  gem "cucumber-rails", require: false
  gem "database_cleaner"
  gem "rr"
  gem "capybara"
  gem "launchy"
  gem 'pickle'
  gem 'simplecov'
  gem 'email_spec'
  gem 'spork'
  gem 'shoulda'
  gem "mocha"
end

#gem "heroku"
gem "thin"
gem 'keepass-password-generator'
gem 'squeel'
