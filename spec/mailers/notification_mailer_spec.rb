require "spec_helper"

describe NotificationMailer, mailer: true do
  it "sends daily_digest_follower to all followers except pledgers" do
  	offer = FactoryGirl.create(:offer)
  	
  	pledger = FactoryGirl.create(:user, name: "pledger")
  	follower = FactoryGirl.create(:user, name: "follower")
  	flower = FactoryGirl.create(:user, name: "flower")
  	noob = FactoryGirl.create(:user, name: "noob")

  	#pledger pledged yesterdy
  	FactoryGirl.create(:pledge, user_id: pledger.id, offer_id: offer.id) do |pledge|
  			pledge.created_at = 2.days.ago 
  			pledge.updated_at = 2.days.ago
  	end

  	#follower followed today
  	follower.follow offer
  	
  	#flower followed 
  	flower.follow offer

  	#flower pledged yesterday
  	FactoryGirl.create(:pledge, user_id: flower.id, offer_id: offer.id) do |pledge|
  			pledge.created_at = 2.days.ago 
  			pledge.updated_at = 2.days.ago
  	end
  	#noob pledged today
  	FactoryGirl.create(:pledge, user_id: noob.id, offer_id: offer.id)

  	list = offer.digestable_followers

  	list.should include(follower)
  	list.should_not include(pledger)
  	list.should_not include(flower)
  	list.should_not include(noob)

  end
end
