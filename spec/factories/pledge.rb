FactoryGirl.define do
  sequence(:random_string) {|n| (0...40).map{ ('a'..'z').to_a[rand(26)] }.join }

  factory :pledge do
  	association :user
  	association :offer
    
    amount 1000
    payment_status :pledged
    
    preapproval_key { generate(:random_string)}
    auth { generate(:random_string) }
    
  end


  factory :payable_pledge, parent: :pledge  do
  	association :payable_offer
  end

  factory :na_pledge, parent: :pledge  do
    payment_status :na
  end

   factory :canceled_pledge, parent: :pledge  do
    payment_status :canceled
  end
  
end