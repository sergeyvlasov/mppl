
FactoryGirl.define do
  factory :user do |user|
    #id 1
    user.name { Faker::Name.name } #'Test User'
    
    #sequence(:email) { |i| "example_#{i}@example.com" } 
    user.email  { Faker::Internet.email }
    
    password "111111"
    password_confirmation "111111"
    
    # required if the Devise Confirmable module is used
    # confirmed_at Time.now
  end

  factory :buyer, parent: :user do
    #name Faker::Name.name
    sequence(:name) { |n| "Buyer#{n}" }
    sequence(:email) { |n| "buyer#{n}@example.com" }
  end


  factory :seller, parent: :user do
    sequence(:name) { |n| "Seller#{n}" }
    sequence(:email) { |n| "seller#{n}@example.com" }
  end


  factory :admin_user, :parent => :user do
    name "admin"
    roles { [ FactoryGirl.create(:admin_role) ] }
  end

  factory :role do
    name  { "Role_#{rand(9999)}" }
  end

  factory :admin_role, :parent => :role do
    name :admin
  end

end