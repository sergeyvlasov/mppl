# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :offer do
    sequence(:title) { |i| "Course#{i}" }
    details 'great course lots of fun'
    min_people 10
    max_people 20
    min_price 2500
    max_price 3500
    pioneer_discount 5 # TODO: review

    image_url "/uploads/karate.png"

    payment_collection_date 1.month.from_now.to_date

    #association :user
    user {FactoryGirl.create(:seller)}
  end


  factory :active_offer, parent: :offer do

    published true

    after(:create) do |offer|

      offer.min_people.times do 
        FactoryGirl.create(:pledge, offer_id: offer.id, amount: offer.max_price)
      end
    end
  end

  factory :payable_offer, parent: :active_offer do
    payment_collection_date Date.today  
  end
end