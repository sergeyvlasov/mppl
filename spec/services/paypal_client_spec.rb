require 'spec_helper'

describe PaypalClient do

  subject(:client) { PaypalClient.new }
  let(:ipn_hash) do
    {
      max_number_of_payments: '1',
      starting_date: '2012-07-15T00:00:19.000-07:00',
      pin_type: 'NOT_REQUIRED',
      max_amount_per_payment: '76.00',
      currency_code: 'ILS',
      sender_email: 'buyer_1341222389_per@gmail.com',
      verify_sign: 'AeTk2GaW9TyDeLSKxqwkLnto5TjnAf-nsPuWAlHnyYWq-7tV.xlJqOys',
      test_ipn: '1',
      date_of_month: '0',
      current_number_of_payments: '0',
      preapproval_key: 'PA-8A842148KC2463158',
      ending_date: '2012-07-15T23:59:19.000-07:00',
      approved: 'true',
      transaction_type: 'Adaptive Payment PREAPPROVAL',
      day_of_week: 'NO_DAY_SPECIFIED',
      status: 'ACTIVE',
      current_total_amount_of_all_payments: '0.00',
      current_period_attempts: '0',
      charset: 'windows-1252',
      payment_period: '0',
      notify_version: 'UNVERSIONED',
      max_total_amount_of_all_payments: '76.00'
    }
  end
  
  let(:ipn_raw) do
    raw_data = ipn_hash.map { |k,v| "#{k}=#{v}" }.join('&')
    puts raw_data
    raw_data
  end

  describe "ipn verification" do
    it "verifies ipn hash" do
      pending #client.verify_ipn(ipn_hash).should == true
    end
    it "verifies raw ipn data" do
      pending #client.verify_ipn(ipn_raw, raw: true).should == true
    end
  end

  

  
end