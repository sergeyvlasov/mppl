require 'rubygems'
require 'spork'
if ENV['COVERAGE']
  require 'simplecov'
  SimpleCov.start 'rails'
end

Spork.prefork do
  # if ENV['COVERAGE']
  #   require 'simplecov'
  #   SimpleCov.start 'rails'
  # end

  ENV["RAILS_ENV"] ||= 'test'
  require File.expand_path("../../config/environment", __FILE__)
  require 'rspec/rails'
  require 'email_spec'
  require 'rspec/autorun'
  require 'database_cleaner'
  require 'pry'
  
  Dir[Rails.root.join("spec/support/**/*.rb")].each {|f| require f}

  RSpec.configure do |config|
    config.include(EmailSpec::Helpers)
    config.include(EmailSpec::Matchers)
    config.include FactoryGirl::Syntax::Methods

    config.color_enabled = true
    config.tty = true
    #config.formatter = :documentation # :progress, :html, :textmate

    config.fixture_path = "#{::Rails.root}/spec/factories"

    config.use_transactional_fixtures = true

    config.infer_base_class_for_anonymous_controllers = false
    config.before(:suite) { DatabaseCleaner.strategy = :truncation }
    config.include(MailerMacros)
    config.before(:each) { reset_email }
  end
  
  if Spork.using_spork? 
    ActiveSupport::Dependencies.clear
    ActiveRecord::Base.instantiate_observers
  end
end

Spork.each_run do
  MPPL::Application.reload_routes!
  FactoryGirl.reload
  DatabaseCleaner.clean
end

