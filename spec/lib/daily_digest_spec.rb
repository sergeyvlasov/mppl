require 'spec_helper'

describe DailyDigest.class do
	
	let!(:mail_stub) { stub(deliver: stub()) }

	# let!(:seller1) { FactoryGirl.create(:seller) }
	# let!(:offer1) { FactoryGirl.create(:offer, user: seller1) }
	
	# let!(:seller2) { FactoryGirl.create(:seller) }
	# let!(:offer2) { FactoryGirl.create(:offer, user: seller2) }
	# let!(:offer3) { FactoryGirl.create(:offer, user: seller2) }
	# let!(:offer4) { FactoryGirl.create(:offer, user: seller2) }

	# let!(:buyer1) { FactoryGirl.create(:buyer) }
	# let!(:buyer2) { FactoryGirl.create(:buyer) }
	# let!(:buyer3) { FactoryGirl.create(:buyer) }
	# let!(:buyer4) { FactoryGirl.create(:buyer) }
	# let!(:buyer5) { FactoryGirl.create(:buyer) }
	
	# let!(:recent_pledge_1) { FactoryGirl.create(:pledge, offer: offer1, user: buyer1) }
	# let!(:recent_pledge_2) { FactoryGirl.create(:pledge, offer: offer1, user: buyer2) }
	# let!(:recent_pledge_3) { FactoryGirl.create(:pledge, offer: offer2, user: buyer3) }
	# let!(:recent_pledge_4) { FactoryGirl.create(:pledge, offer: offer4, user: buyer5) }

	# let!(:aged_pledge_1) { FactoryGirl.create(:pledge, offer: offer1, user: buyer1, created_at: 2.days.ago) }
	# let!(:aged_pledge_2) { FactoryGirl.create(:pledge, offer: offer1, user: buyer3, created_at: 2.days.ago) }
	# let!(:aged_pledge_3) { FactoryGirl.create(:pledge, offer: offer2, user: buyer4, created_at: 2.days.ago) }
	# let!(:aged_pledge_4) { FactoryGirl.create(:pledge, offer: offer3, user: buyer5, created_at: 2.days.ago) }

	describe "#daily_digest_buyer" do
		
		context "when there is a recent pledge" do 
			before do
				recent_pledge = FactoryGirl.create(:pledge)
				recent_buyer = recent_pledge.user
				@offer = recent_pledge.offer
				
				aged_pledge = FactoryGirl.create(:pledge, offer_id: @offer.id, created_at: 2.days.ago)
				@aged_buyer = aged_pledge.user
			end
			context "and an aged pledge on the same offer" do 
				it "should send 1 email to aged_pledger" do
					NotificationMailer.should_receive(:daily_digest_buyer)
														.with(@offer, @aged_buyer)
														.exactly(1).times
														.and_return(mail_stub)
					DailyDigest.daily_digest_buyer
				end
				context "and another recent pledge on the same offer" do
					it "should send 1 email to aged_pledger" do
						NotificationMailer.should_receive(:daily_digest_buyer)
														.with(@offer, @aged_buyer)
														.exactly(1).times
														.and_return(mail_stub)
						DailyDigest.daily_digest_buyer
					end
				end
			end
		end
		# it "should contain specific combinations of offer_id and user_id" do
		# 	mapped_result = DailyDigest.daily_digest_buyer_receivers
		# 	#binding.pry
		# 	mapped_result.should have_exactly(2).items
		# 	mapped_result.should include(user_id: buyer3.id, offer_id: offer1.id)
		# 	mapped_result.should include(user_id: buyer4.id, offer_id: offer2.id)
		# end

		context "when there is :na or :canceled pledge" do
			it "doesn't changes receivers count" do
				expect { FactoryGirl.create(:na_pledge) }.to_not change { DailyDigest.daily_digest_buyer_receivers.count }
			end
			it "doesn't changes receivers count" do
				expect { FactoryGirl.create(:canceled_pledge) }.to_not change { DailyDigest.daily_digest_buyer_receivers.count }
			end
			it "doesn't found :na, :canceled pledges" do
				Pledge.where(payment_status: Pledge.ps_idx([:na, :canceled])).count.should == 0
			end
		end

		context "when there is a buyer which pledged twice on the same offer" do
			before do
				@offer = FactoryGirl.create(:offer)
				@double_pledger = FactoryGirl.create(:buyer, name: "Double Pledger")
				fresh_pledger = FactoryGirl.create(:buyer)
				pledge_hash = {
					offer_id: @offer.id, 
					user_id: @double_pledger.id, 
					amount: @offer.max_price,
					created_at: 2.days.ago,
					updated_at: 2.days.ago
				}
				aged_pledge1 = FactoryGirl.create(:pledge, pledge_hash)
				aged_pledge2 = FactoryGirl.create(:pledge, pledge_hash)
				recent_pledge = FactoryGirl.create(:pledge, offer_id: @offer.id, user_id: fresh_pledger, amount: @offer.max_price)
			end

			let(:all_receivers) { DailyDigest.daily_digest_buyer_receivers }
			let(:double_pledger_hash) do { user_id: @double_pledger.id, offer_id: @offer.id } end

			it "sends digest to double pledger" do
				all_receivers.should include(double_pledger_hash)
			end

			it "sends digest to double pledger only once" do
				counts = Hash.new(0)
				all_receivers.each do |obj|
					counts[obj] += 1
				end
				counts[double_pledger_hash].should == 1
			end
		end
		
	end




	describe "daily_digest_seller" do
	end
end