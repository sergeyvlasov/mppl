#TODO
#now: :na -> cancel , send emails(admin, seller, buyer)
#future: :expired pledges -> check ending date!!! 
## ppak status matrix:
# approved 	| status 		| scenario						|	do with :na
#--------------------------------------------- -------------
#  true			|	ACTIVE 		| clicked "approve" 	| activate
#----------------------------------------				---------
#  true			| CANCELED 	| cancelled via PP UI	| cancel
#------------------------------------------			---------			
#  false		|	ACTIVE 		|	leaved(did nothing) | destroy
#----------------------------------------------	----------
#  false  	| CANCELED 	| clicked "cancel" 		| destroy
require 'spec_helper'

def mock_paypal
	PaypalAdaptive::Request.any_instance.should_receive(:preapproval_details).and_return(pay_response)
	PaypalAdaptive::Request.any_instance.stub(:cancel_preapproval)
end

describe PPAClient do

	describe "#sync_pledges" do
		let!(:response_envelope) do { "responseEnvelope" => { "ack" => "Success" } } end
		let!(:offer) { FactoryGirl.create(:offer, payment_collection_date: Date.today + 1 ) }
		let!(:pledge) { FactoryGirl.create(:pledge, { offer_id: offer.id, payment_status: :na}) }
		
		let!(:ppac) { PPAClient.new }
		
			
		describe "a pledge with :na payment_status" do
			context "when the pledge was not approved via Paypal" do
				let!(:pay_response) do
					{
						"approved" => "false",
						"status" => "ACTIVE"
					}.merge(response_envelope)
				end

				it "it should be canceled" do
					mock_paypal
					ppac.sync_pledges
					pledge.reload.payment_status.should == :canceled
					#should send bouncy email to seller
				end

				describe "'Bounced Pledge' email " do
					it "bounced pledge notification email should be sent" do
						pending
						# mock_paypal
						# NotificationMailer.should_receive(:bounced_pledge_seller).
						# ppac.sync_pledges
					end
					it "it should be sent to Seller" do
						mock_paypal
						ppac.sync_pledges
						ActionMailer::Base.deliveries.last.to.should include(pledge.offer.user.email) 
					end
				end

				it "'Please retry your pledge' email notification should be sent to Buyer" do
					pending
				end
	
				it "'Investigation needed' email notification should be sent to Admin" do
					pending
				end
			end

			context "when a pledge was approved via PayPal" do
				let!(:pay_response) do
					{
						"approved" => "true",
						"status" => "ACTIVE"
					}.merge(response_envelope)
				end

				it "should be activated" do
					PaypalAdaptive::Request.any_instance.should_receive(:preapproval_details).and_return(pay_response)
					PaypalAdaptive::Request.any_instance.stub(:cancel_preapproval)
					ppac.sync_pledges

					pledge.reload.payment_status.should == :pledged
				end
			end
		
		end

		
		context "when a pledge is recognized by PayPal as approved/canceled" do
			it "should be canceled" do
				pending
			end
		end

		context "when a pledge is recognized by PayPal as not approved/canceled" do
			it "should be deleted" do
				pending
			end
		end
	end 


	describe "#collect_pledge_payment" do
		before(:each) do
			#PaypalAdaptive::Response.any_instance.stub(:initialize)
			pay_response = {} #PaypalAdaptive::Response.new
			pay_response.stub(:success?).and_return(true)
			PaypalAdaptive::Request.any_instance.stub(:pay).and_return(pay_response)
		end
		
		let(:payable_offer) { FactoryGirl.create(:payable_offer) }

		describe "when the pledge is non payable" do
			#it should ignore
			let(:active_offer) { FactoryGirl.create(:active_offer) }
			specify "it should ignore pledge" do
				active_pledge = active_offer.pledges.first
				expect { subject.collect_pledge_payment active_pledge }.not_to change{ Payment.count }
			end
		end

		specify "it should create a payment" do
			payable_pledge = payable_offer.pledges.first
			expect { subject.collect_pledge_payment payable_pledge }.to change{ Payment.count }.from(0).to(1)
		end

	end

		it "ignores all non NA pledges" do
			pending	
		end

		it "ignores all pledges with no preapproval_key" do
			pending
		end
		
		it "synchronizes all NA pledges" do
			pending
		end

end