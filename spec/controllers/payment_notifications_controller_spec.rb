require 'spec_helper'

describe PaymentNotificationsController, ipn: true do
  
  it "PaymentNotificationsController should have [ACTIVE, CANCELED] constants defined" do
    PaymentNotificationsController::ACTIVE.should == "ACTIVE"
    PaymentNotificationsController::CANCELED.should == "CANCELED"
  end

  describe "action #create" do

    let(:preapproval_key) { "DUMMYPREAPPROVALKEY" }

    let(:valid_params) do
      { 
        preapproval_key: preapproval_key,
        approved: "true",
        status: "ACTIVE",
        sender_email: "gm_1231902590_per@paypal.com"
      } 
    end

    before(:each) do
      FactoryGirl.create(:pledge, preapproval_key: preapproval_key)
      PaypalClient.any_instance.stub(:verify_ipn).and_return(true)
    end

    describe "valid user not logged in" do
      let!(:pak) { "ANOTHER_PAK" }
      let!(:email) { "paypal@paypal.com" }
      let!(:payer) { FactoryGirl.create(:user, email: email) }
      let!(:pledge) { FactoryGirl.create(:pledge, user_id: nil, preapproval_key: pak) }
      before do
        valid_params[:sender_email] = email
        valid_params[:preapproval_key]= pak
      end

      it "should assign pledge to existing user" do
        post :create, valid_params
        pledge.reload.user.should == payer
      end
    end
    
    ############################################
    ## record creation
    it "creates an IPN record" do
      expect { post :create, valid_params }.to change{ PaymentNotification.count }.from(0).to(1)
    end

    it "should save :params as is" do
      post :create, valid_params
      ipn = PaymentNotification.first
      ipn.preapproval_key.should == valid_params[:preapproval_key]
      ipn.approved.to_s.should == valid_params[:approved]
      ipn.status.should == valid_params[:status]
    end
    #############################################
    ## Security Checks
    # it "verifies IPN record against PayPal" do
    #   PaypalClient.any_instance.should_receive(:verify_ipn).and_return(true)
    #   post :create, valid_params
    # end

    it "does additional checks against the db" #(see railscast)
    #############################################
    ## Pledge Activation/Cancellation


    it "tries to finds pledge with the same preapproval key" do
      pledge = Pledge.first
      Pledge.should_receive(:find_by_preapproval_key).with(preapproval_key).and_return(pledge)
      post :create, valid_params
    end
        
    context "when pledge has no user" do
                
      context "there is no user with email == sender_email" do
        before(:each) do
          pledge = Pledge.first
          pledge.user_id = nil
          pledge.save
        end


        it "creates a user by email=sender_email if the pledge has no user" do
          sender_email = valid_params[:sender_email]
          User.find_by_email(sender_email).should == nil
          expect { post :create, valid_params }.to change { User.count }.by(1)
        end
      end

      context "there is already a user with email == sender_email" do
        it "finds a user by email=sender_email if the pledge has no user" do
          expect { post :create, valid_params }.to change { User.count }.by(0)
        end
      end

      
      it "associates the pledge with the user"
    end
    
    # if pledge has user
    # if status == ACTIVE and approved == true
    # activate the pledge
    it "sets payment_status pledged" do
      Pledge.first.payment_status.should == :pledged
    end
    it "sends activation notifications to buyer and seller"
    # cancel the pledge
    context "when status==CANCELED" do
      let(:valid_params) do
        { 
          preapproval_key: preapproval_key,
          approved: "true",
          status: "CANCELED",
          sender_email: "gm_1231902590_per@paypal.com"
        } 
      end
      
      it "sets payment_status cancelled" do
        post :create, valid_params
        Pledge.first.payment_status.should == :canceled
      end
      it "sends cancellation notifications to buyer and seller"
    end
   
    it "renders nothing" do
        post :create
        response.status.should == 200
        response.body.should == " "
    end


  end

  
end
