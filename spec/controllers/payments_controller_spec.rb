require 'spec_helper'

describe PaymentsController do

  let!(:buyer) { FactoryGirl.create :user }
  let!(:seller) { FactoryGirl.create :user }
  let!(:offer) { FactoryGirl.create :offer, user: seller }
  let!(:pledge) { FactoryGirl.create :pledge, offer: offer }
  let!(:valid_attributes) do
      {
        pledge_id: pledge.id,
        amount: 10, 
        currency: "NIS"
      }
  end
  let!(:payment) { Payment.create valid_attributes }

  before do
    sign_in buyer
  end

  describe "GET index" do
    it "assigns all payments as @payments" do
      
      payment.should be_valid
      payment.pledge.should be
      payment.pledge.offer.should be

      get :index, {offer_id: offer}
      assigns(:payments).should eq([payment])
    end
  end

  describe "GET show" do
    it "assigns the requested payment as @payment" do
      payment = Payment.create! valid_attributes
      get :show, {offer_id: offer, :id => payment.to_param}
      
      assigns(:payment).should eq(payment)
      
    end
  end

  describe "GET new" do
    it "assigns a new payment as @payment" do
      get :new, {offer_id: offer}
      assigns(:payment).should be_a_new(Payment)
    end
  end

  # describe "GET edit" do

  #   it "redirects to home page with an error message" do
  #       payment = Payment.create! valid_attributes
  #       get :edit, {offer_id: offer, :id => payment.to_param}
  #       response.should redirect_to root_path
  #   end

  #   it "assigns the requested payment as @payment" do
  #     payment = Payment.create! valid_attributes
  #     get :edit, {offer_id: offer, :id => payment.to_param}

  #     pp payment: payment
  #     pp assigns: assigns

  #     assigns(:payment).should eq(payment)
  #   end
  # end

  describe "POST create" do
    describe "with valid params" do
      it "creates a new Payment" do
        expect {
          post :create, {offer_id: offer, :payment => valid_attributes}
        }.to change(Payment, :count).by(1)
      end

      it "assigns a newly created payment as @payment" do
        post :create, {offer_id: offer, :payment => valid_attributes}
        assigns(:payment).should be_a(Payment)
        assigns(:payment).should be_persisted
      end

      # it "redirects to the created payment" do
      #   post :create, {offer_id: offer, :payment => valid_attributes}
      #   response.should redirect_to(Payment.last)
      # end
    end

    describe "with invalid params" do
      it "assigns a newly created but unsaved payment as @payment" do
        # Trigger the behavior that occurs when invalid params are submitted
        Payment.any_instance.stub(:save).and_return(false)
        post :create, {offer_id: offer, :payment => {}}
        assigns(:payment).should be_a_new(Payment)
      end

      it "re-renders the 'new' template" do
        # Trigger the behavior that occurs when invalid params are submitted
        Payment.any_instance.stub(:save).and_return(false)
        post :create, {offer_id: offer, :payment => {}}
        response.should render_template("new")
      end
    end
  end

  # describe "PUT update" do
  #   describe "with valid params" do
  #     it "updates the requested payment" do
  #       payment = Payment.create! valid_attributes
  #       # Assuming there are no other payments in the database, this
  #       # specifies that the Payment created on the previous line
  #       # receives the :update_attributes message with whatever params are
  #       # submitted in the request.
  #       Payment.any_instance.should_receive(:update_attributes).with({'these' => 'params'})
  #       put :update, {offer_id: offer, :id => payment.to_param, :payment => {'these' => 'params'}}
  #     end

  #     it "assigns the requested payment as @payment" do
  #       payment = Payment.create! valid_attributes
  #       put :update, {offer_id: offer, :id => payment.to_param, :payment => valid_attributes}
  #       assigns(:payment).should eq(payment)
  #     end

  #     it "redirects to the payment" do
  #       payment = Payment.create! valid_attributes
  #       put :update, {offer_id: offer, :id => payment.to_param, :payment => valid_attributes}
  #       response.should redirect_to(payment)
  #     end
  #   end

  #   describe "with invalid params" do
  #     it "assigns the payment as @payment" do
  #       payment = Payment.create! valid_attributes
  #       # Trigger the behavior that occurs when invalid params are submitted
  #       Payment.any_instance.stub(:save).and_return(false)
  #       put :update, {offer_id: offer, :id => payment.to_param, :payment => {}}
  #       assigns(:payment).should eq(payment)
  #     end

  #     it "re-renders the 'edit' template" do
  #       payment = Payment.create! valid_attributes
  #       # Trigger the behavior that occurs when invalid params are submitted
  #       Payment.any_instance.stub(:save).and_return(false)
  #       put :update, {offer_id: offer, :id => payment.to_param, :payment => {}}
  #       response.should render_template("edit")
  #     end
  #   end
  # end

  # describe "DELETE destroy" do
  #   it "destroys the requested payment" do
  #     payment = Payment.create! valid_attributes
  #     expect {
  #       delete :destroy, {offer_id: offer, :id => payment.to_param}
  #     }.to change(Payment, :count).by(-1)
  #   end

  #   it "redirects to the payments list" do
  #     payment = Payment.create! valid_attributes
  #     delete :destroy, {offer_id: offer, :id => payment.to_param}
  #     response.should redirect_to(payments_url)
  #   end
  # end
  
end
