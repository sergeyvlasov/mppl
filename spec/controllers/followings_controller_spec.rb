require 'spec_helper'

describe FollowingsController do


  describe "POST 'create'" do

    let(:offer) { FactoryGirl.create(:offer) }
    let(:user) { FactoryGirl.create(:user) }

    let(:following_hash) do
      {
        offer_id: offer.id,
        user_id: user.id
      }
    end

    context "no same following was created before" do
      it "should create a following by params" do
        #TODO:
        pending
        # post 'create', { following: following_hash }
        # following = assigns(:following)
        # following.try(:offer_id).should == offer.id
        # following.try(:user_id).should == user.id
      end

      it "should increase count of followings" do
        #TODO:
        pending
        #post 'create', { following: following_hash }
        
        #expect { post 'create', { following: following_hash } }.to change { Following.count }.from(0).to(1)
          
        #pp "Followings: #{Following.count}"
        
        # Following.count.should == 1

      end
    end

    context "same following was created before" do

      before(:each) do
        Following.create(following_hash)
      end

      it "should find a following by params" do
        #TODO:
        pending
        # post 'create', { following: following_hash }
        # following = assigns(:following)
        # following.try(:offer_id).should == offer.id
        # following.try(:user_id).should == user.id
      end

      it "should not change count of followings" do
        #TODO:
        pending
        # Following.count.should == 1
        # expect { post 'create', { following: following_hash }  }.to_not change { Following.count }
        # Following.count.should == 1

      end
    end  
    
  end

end