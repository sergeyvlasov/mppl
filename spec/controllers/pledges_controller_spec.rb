require 'spec_helper'

describe PledgesController do

  let!(:buyer) do 
    FactoryGirl.create :user
  end

  let!(:seller) do 
    FactoryGirl.create :user
  end

  let!(:user) do 
    FactoryGirl.create :user
  end

  let!(:offer) { FactoryGirl.create :offer, user: seller }
  let!(:valid_params) do
    { 
      offer: offer, 
      user: buyer, 
      amount: offer.min_price, 
      payment_status:  :pledged
    } 
  end
  let!(:pledge) do 
    FactoryGirl.create :pledge, valid_params
  end
  
  
  
  context "Guest User Can" do
  end

  context "Guest User Cannot" do
  end

  context "Logged In User" do

    before do
      sign_in user
    end #
    
    describe "GET index" do
      it "assigns available pledges as @pledges" do
        get :index, {offer_id: offer.id}
        assigns(:pledges).should eq([pledge])
      end #
    end #
  end

  context "Buyer Signed In" do
    before do
      sign_in buyer
    end


    describe "Pledge#create", create_pledge: true do

      def paypal_mock(success = true)
        @preapproval_key = "DUMMYPREAPPROVALKEY"
        @paypal_url = "https://sandbox.paypal.com/webscr?cmd=_ap-preapproval&preapprovalkey=#{@preapproval_key}"
        
        paypal_response = double("PaypalAdaptive::Response")
        
        PaypalAdaptive::Request.any_instance.should_receive(:preapproval).and_return(paypal_response)
 
        paypal_response.should_receive(:success?).and_return(success)
        if(success)
          paypal_response.should_receive(:[]).with("preapprovalKey").and_return(@preapproval_key)
          paypal_response.should_receive(:preapproval_paypal_payment_url).and_return(@paypal_url)  
        end
      end

      describe "paypal response successful" do
        before(:each) do
          paypal_mock
        end

        it "creates a pledge" do
          #creates a new Pledge
          expect {
            post :create, {offer_id: offer.id, :pledge => { amount: offer.max_price } }
          }.to change(Pledge, :count).by(1)
        end

        it "assigns a newly created pledge as @pledge" do
          post :create, {offer_id: offer.id, :pledge => { amount: offer.max_price } }
          assigns(:pledge).should be_a(Pledge)
          assigns(:pledge).should be_persisted
        end

        it "redirects to Paypal" do
          post :create, {offer_id: offer.id, :pledge => { amount: offer.max_price } }
          response.should redirect_to(@paypal_url)
        end

      end

      describe "paypal response unsuccessful" do
        before(:each) do
          paypal_mock(false)
        end

        it "doesn't create any pledge" do
          #creates a new Pledge
          expect {
            post :create, {offer_id: offer.id, :pledge => { amount: offer.max_price } }
          }.to change(Pledge, :count).by(0)
        end     
      end

    end #Pledge#create

  end #Buyer signed in

  context "Seller signed in" do
    before do
      sign_in seller
    end

    # TODO
  end

  context "Admin signed in" do
    # TODO
  end
    
  
 describe "GET show" do
    it "assigns the requested pledge as @pledge" do
      pending
      pledge = Pledge.create! valid_params
      get :show, {offer_id: offer, :id => pledge.to_param}
      assigns(:pledge).should eq(pledge)
    end
  end

  describe "GET new" do
    it "assigns a new pledge as @pledge" do
      # get :new, {offer_id: offer}
      # assigns(:pledge).should be_a_new(Pledge)
    end
  end

  describe "GET edit" do
    it "assigns the requested pledge as @pledge" do
      #TODO
      pending
      # pledge = Pledge.create! valid_params
      # get :edit, {offer_id: offer, :id => pledge.to_param}
      # assigns(:pledge).should eq(pledge)
    end
  end



  describe "POST create" do
    describe "with invalid params" do
      it "assigns a newly created but unsaved pledge as @pledge" do
        # Trigger the behavior that occurs when invalid params are submitted
        Pledge.any_instance.stub(:save).and_return(false)
        post :create, {offer_id: offer, :pledge => {}}
        assigns(:pledge).should be_a_new(Pledge)
      end

      it "re-renders the 'new' template" do
        # Trigger the behavior that occurs when invalid params are submitted
        Pledge.any_instance.stub(:save).and_return(false)
        post :create, {offer_id: offer, :pledge => {}}
        response.should render_template("new")
      end
    end
  end

  describe "PUT update" do
    describe "with valid params" do
      it "updates the requested pledge", failed: true  do
        # Assuming there are no other pledges in the database, this
        # specifies that the Pledge created in the let!(:pledge) block
        # receives the :update_attributes message with whatever params are
        # submitted in the request.

        #TODO:
        pending
        # Pledge.any_instance.should_receive(:update_attributes).with({'these' => 'params'})
        # put :update, {offer_id: offer, :id => pledge.to_param, :pledge => {'these' => 'params'}}
      end

      it "assigns the requested pledge as @pledge" do
        #TODO:
        pending
        # pledge = Pledge.create! valid_params
        # put :update, {offer_id: offer, :id => pledge.to_param, :pledge => valid_params}
        # assigns(:pledge).should eq(pledge)
      end

      it "redirects to the pledge", redirect: true  do
        #TODO:
        pending
        # sign_in buyer
        # put :update, {offer_id: offer, :id => pledge.to_param, :pledge => valid_params}
        # response.should redirect_to(offer_pledge_path(offer, pledge)) # TODO review
      end
    end

    describe "unauthorized access" do
      it "redirects to login page", redirect: true do
        #TODO:
        pending
        # put :update, {offer_id: offer, :id => pledge.to_param, :pledge => valid_params}
        # response.should redirect_to(new_user_session_path)
      end
    end

    describe "with invalid params" do
      it "assigns the pledge as @pledge" do
        #TODO:
        pending
        # pledge = Pledge.create! valid_params
        # # Trigger the behavior that occurs when invalid params are submitted
        # Pledge.any_instance.stub(:save).and_return(false)
        # put :update, {offer_id: offer, :id => pledge.to_param, :pledge => {}}
        # assigns(:pledge).should eq(pledge)
      end

      it "re-renders the 'edit' template", failed: true  do
        #TODO:
        pending
        # pledge = Pledge.create! valid_params
        # # Trigger the behavior that occurs when invalid params are submitted
        # Pledge.any_instance.stub(:save).and_return(false)
        # put :update, {offer_id: offer, :id => pledge.to_param, :pledge => {}}
        # response.should render_template("edit")
      end
    end
  end

  describe "DELETE destroy" do
    it "destroys the requested pledge", failed: true  do
      #TODO:
        pending
      # pledge = Pledge.create! valid_params
      # expect {
      #   delete :destroy, {offer_id: offer, :id => pledge.to_param}
      # }.to change(Pledge, :count).by(-1)
    end

    it "redirects to the pledges list", failed: true  do
      #TODO:
        pending
      # pledge = Pledge.create! valid_params
      # delete :destroy, {offer_id: offer, :id => pledge.to_param}
      # response.should redirect_to(offer_pledges_path(offer)) # TODO review
    end
  end

end