class Matcher
  def initialize(attribute)
    @attribute = attribute
    @message = "can't be blank"
  end

  def matches?(model)
    #collect errors and find a match
    @model = model 
    @model.valid?
    errors = @model.errors[@attribute]
    errors.any? { |error| error == @message }
  end

  def with_message(message)
    @message = message
    self
  end
end
