require 'spec_helper'

describe "Definite Pledge" do

	

	#let(:offer_options) do { max_price: 200, min_people: 5, min_price: 100, max_people: 25 } end

	before(:each) do
		Pledge.destroy_all
		Offer.destroy_all
		User.destroy_all

		@offer = FactoryGirl.create(:offer, { max_price: 200, min_people: 5, min_price: 100, max_people: 25 })
	end
	
	context "Offer has several pledges" do
		
		specify "case 1: next active pledge price is max_price" do
			#steps = 21 (0..20)
			#price_step = 5
			#prices = [200,195,190,...110,105,100]
			#people = [5,  6,  7,...   23, 24, 25]
			#price(people) = -> { C - people * step }
			pledge_amounts = [ 190, 190 ]

			################################################################################

			pledge_amounts.each do |amount|
				FactoryGirl.create(:pledge, {amount: amount, offer_id: @offer.id})
			end
			
			@offer.calculate_current_price
			@offer.current_price.should == 200
			@offer.next_pledge_price.should == 200

		end

		specify "case 2: next active pledge price is one step down from current_price" do
			
			#steps = 21 (0..20)
			#price_step = 5
			#prices = [200,195,190,...110,105,100]
			#people = [5,  6,  7,...   23, 24, 25]
			#price(people) = -> { C - people * step }
			pledge_amounts = [ 200, 200, 200, 200, 200, 195 ]
			################################################################################

			pledge_amounts.each do |amount|
				FactoryGirl.create(:pledge, {amount: amount, offer_id: @offer.id})
			end
			
			@offer.pledges.count.should == pledge_amounts.size

			@offer.calculate_current_price
			current_price = @offer.current_price
			
			current_price.should == 195
			@offer.insiders(current_price).length.should == 6

			#next_price should be 190
			@offer.next_pledge_price.should == 190

		end

		


	end

	

end