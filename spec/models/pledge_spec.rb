require 'spec_helper'

describe Pledge do

  before(:all) do
    # valid attributes
    @attr = {
      user_id: 1,
      offer_id: 1,
      amount: 1000,
    }
  end

  let(:pledge) { FactoryGirl.create :pledge }

  describe "when payment_status is :cash" do
    let(:seller) { FactoryGirl.create(:seller) }
    let(:buyer){ FactoryGirl.create(:buyer) }
    let(:offer) { FactoryGirl.create(:offer, user_id: seller.id) }
   
    context "should be valid with a buyer" do
      subject(:pledge) { Pledge.create(payment_status: :cash, offer_id: offer.id, amount: offer.current_price, user_id: buyer.id) }
      it { should be_valid }
    end
  end

  describe "Payment Statuses" do
    it "doesn't allow to store a non-existent payment status", failed: true  do
      #TODO:
      pending
      # pledge.payment_status = :be_my_guest
      # pledge.should_not be_valid
    end
  end

  describe "valid creation" do
    it "should create a new instance given a valid attribute" do
      Pledge.create!(@attr)
    end

    it "should create only one pledge per user per offer" do
      pledge = Pledge.create!(@attr)
      new_pledge = Pledge.new(user_id: pledge.user_id, offer_id: pledge.offer_id)
      new_pledge.should_not be_valid
    end
  end

  describe "pledge authentication" do
    before(:each) do
      @pledge = Pledge.create!(@attr)
  	end

    it "should have authentication uniq key" do
      @pledge.auth.should_not be_nil
    end

    it "should authenticate uniq key" do
      auth = @pledge.auth
      @pledge.authenticate(auth).should be_true
    end

    it "should not authenticate incorrect uniq key" do
      auth = @pledge.auth
      auth = auth.reverse
      @pledge.authenticate(auth).should be_false
    end
  end

end
