require 'spec_helper'

describe Payment do
  	
  	let!(:offer) { FactoryGirl.create(:offer) }
	let!(:pledge) { FactoryGirl.create(:pledge, { offer: offer}) }
	let!(:valid_attributes) { 
		{
			pledge_id: pledge.id,
			amount: 10.0,
			currency: "NIS"
		} 
	}
	let(:valid_payment) { Payment.new valid_attributes }

	describe "dummy payment" do
		it { should_not be_valid }
	end

	describe "valid payment" do
		pending
	end

end
