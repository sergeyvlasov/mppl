require File.dirname(__FILE__) + '/../spec_helper'

describe PaymentNotification do
  it "should be valid" do
    PaymentNotification.new.should be_valid
  end

  it { should respond_to :params }
  it { should respond_to :preapproval_key }
  it { should respond_to :approved }
  it { should respond_to :status }
  it { should respond_to :sender_email }
end
