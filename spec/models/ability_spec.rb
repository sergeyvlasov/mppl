require "spec_helper"
require "cancan/matchers"

describe "User abilities" do  
  
  context "when user is a guest" do
    subject { ability }

    let(:user){ User.new }
    let(:ability){ Ability.new(user) }

    describe "accessing Offers," do
  		it "then user should be able to view offer list" do
				ability.should be_able_to(:read, Offer)
			end

			it "then user should not be able to create an offer" do
				ability.should_not be_able_to(:create, Offer)
			end

  	end
    	
  	describe "accessing Pledges," do
  		it "then user should not be able to view pledge list" do
		    ability.should_not be_able_to(:read, Pledge)
	    end


			it "then user should be able to create a pledge" do
        ability.should be_able_to(:create, Pledge)
			end
   	end

	end

  context "when user is logged in" do
    subject { ability }

    let!(:user){ FactoryGirl.create(:user) }
    let!(:ability){ Ability.new(user) }    

    it "should create new Offer" do
      # TODO ability.should be_able_to(:create, Offer)
    end

    it "should be able to create new Pledge" do
      ability.should be_able_to(:create, Pledge)
    end

    context "and Pledge owner" do
      let(:pledge_with_user) do 
        FactoryGirl.create(:pledge, user: user) 
      end

      it "user should own the pledge" do
        user.should eq pledge_with_user.user
      end

      it "should be able to update own Pledge" do
        ability.should be_able_to(:update, pledge_with_user)
      end

      it "should be able to destroy own Pledge" do
        ability.should be_able_to(:destroy, pledge_with_user)
      end
    end

    context "and the pledge belongs to other user" do
      let(:other_user) { FactoryGirl.create(:buyer) }
      let(:pledge_with_user) { FactoryGirl.create(:pledge, user: other_user) }
      
      it "should not be able to update it" do
        ability.should_not be_able_to(:update, pledge_with_user)
      end

      it "should not be able to destroy it" do
        ability.should_not be_able_to(:destroy, pledge_with_user)
      end
    end

  end
end