require 'spec_helper'

describe "Offer Prices" do

	before(:each) do 
		User.destroy_all
	end

	specify "xxxxxxxxx" do
		@pledge = FactoryGirl.create(:pledge)
		@pledge.should_not 			be_nil
		@pledge.should 				be_valid
		@pledge.offer.should_not 	be_nil
		@pledge.offer.should 		be_valid
		@pledge.user.should_not 	be_nil
		@pledge.user.should 		be_valid
		@pledge.offer.user.should_not 	be_nil
		@pledge.offer.user.should 		be_valid
		@pledge.user.id.should_not == @pledge.offer.user.id
	end

	it "should calculate pledge price for people" do
		offer_params = { min_people: 2, max_price: 10, max_people: 6, min_price: 6 }
		@offer = FactoryGirl.create(:offer, offer_params)
		@offer.pledge_price_for_people(0).should == 10
		@offer.pledge_price_for_people(1).should == 10
		@offer.pledge_price_for_people(2).should == 10
		@offer.pledge_price_for_people(3).should == 9
		@offer.pledge_price_for_people(4).should == 8
		@offer.pledge_price_for_people(5).should == 7
		@offer.pledge_price_for_people(6).should == 6
		@offer.pledge_price_for_people(7).should == 6
		@offer.pledge_price_for_people(8).should == 6
	end

	it "should calculate current offer price" do
		offer_params = { min_people: 2, max_price: 10, max_people: 6, min_price: 6 }
		@offer = FactoryGirl.create(:offer, offer_params)
		pledge_amounts = [ 10, 9, 8, 8, 6 ]
		pledges = pledge_amounts.map do |amount|
			pledge = FactoryGirl.create(:pledge, { offer_id: @offer.id, amount: amount })
		end

		@offer.current_offer_price.should == 8
	end
	
	specify "case 1: next pledge price" do
		offer_params = { min_people: 2, max_price: 10, max_people: 6, min_price: 6 }
		@offer = FactoryGirl.create(:offer, offer_params)
		#people/price

		pledge_amounts = [ 10, 9, 8, 6 ]

		pledges = pledge_amounts.map do |amount|
			pledge = FactoryGirl.create(:pledge, { offer_id: @offer.id, amount: amount })
		end

		@offer.next_pledge_price.should == 8
	end

	specify "case 2: next pledge price" do
		offer_params = { min_people: 2, max_price: 100, max_people: 6, min_price: 60 }
		@offer = FactoryGirl.create(:offer, offer_params)
		#people/price

		pledge_amounts = [ 100, 90, 80, 60 ]
		pledges = pledge_amounts.map do |amount|
			pledge = FactoryGirl.create(:pledge, { offer_id: @offer.id, amount: amount })
		end

		@offer.next_pledge_price.should == 80		
	end

	specify "case 3: next pledge price" do
		offer_params = { min_people: 10, max_price: 3500, max_people: 20, min_price: 2500 }
		@offer = FactoryGirl.create(:offer, offer_params)
		#people/price

		pledge_amounts = []

		12.times {  pledge_amounts << 3300 }
		2.times { pledge_amounts << 3000  }

		pledges = pledge_amounts.map do |amount|
			pledge = FactoryGirl.create(:pledge, { offer_id: @offer.id, amount: amount })
		end
		
		@offer.current_offer_price.should == 3300
		@offer.next_pledge_price.should == 3000
	end

end