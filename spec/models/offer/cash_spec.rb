require 'spec_helper'

describe "Cash Payments" do 

	before(:each) do 
		User.destroy_all
		Offer.destroy_all
		Pledge.destroy_all
		Payment.destroy_all

		@offer = FactoryGirl.create(:offer)
	end
	
	specify "pending offer should accept cash" do
		
		@offer.should be_pending
		@offer.should be_accepting_cash
	end

	specify "active offer with maximum people should not accept cash" do
		@offer.max_people.times do 
			pledge = FactoryGirl.create(:pledge, { offer_id: @offer.id, amount: @offer.max_price })
		end

		@offer.should be_active
		@offer.should_not be_accepting_cash
	end

	specify "active offer with pledge count less than max people should accept cash" do
		(@offer.max_people - 1).times do 
			pledge = FactoryGirl.create(:pledge, { offer_id: @offer.id, amount: @offer.max_price })
		end


		@offer.should be_active
		@offer.should be_accepting_cash
	end

	
  	it "active non-cash pledge should be payable" do
    	@offer.min_people.times do
    		pledge = FactoryGirl.create(:pledge, { offer_id: @offer.id, amount: @offer.max_price })
    	end

    	
    	pledge = @offer.pledges.first
    	pledge.should be_active
    	pledge.should be_payable

    	@offer.payable_pledges.should include(pledge)
  	end
	
end