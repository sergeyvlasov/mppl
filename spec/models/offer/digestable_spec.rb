require 'spec_helper'

describe Offer do

	before do
		@offer = FactoryGirl.create(:offer)
	end


	describe "#digestable" do
		def expect_include offer
			Offer.digestable.should include offer
		end
		def expect_exclude offer
			Offer.digestable.should_not include offer
		end

		describe "when offer has recent pledges" do
			before do
				FactoryGirl.create(:pledge, offer_id: @offer.id)
			end
			it "includes the offer" do
				expect_include @offer
			end
			it "when it has an aged pledge -> still includes the offer" do
				FactoryGirl.create(:pledge, offer_id: @offer.id, created_at: 2.days.ago)
				expect_include @offer
			end
		end
		
		describe "when offer has no recent pledges" do
			it "does not include the offer" do
				expect_exclude @offer
			end
			it "when it has an aged pledge -> still does not include the offer" do
				FactoryGirl.create(:pledge, offer_id: @offer.id, created_at: 2.days.ago)
				expect_exclude @offer
			end
			it "when it has a recent N/A pledge -> still does not include the offer" do
				FactoryGirl.create(:pledge, offer_id: @offer.id, payment_status: :na)
				expect_exclude @offer
			end
		end
	end

	describe "#digestable_followers" do
		before do
			@follower = FactoryGirl.create(:buyer)
			@follower.follow @offer
		end
		it "does not include follower which is also a pledger" do
			FactoryGirl.create(:pledge, user_id: @follower.id, offer_id: @offer.id)
			@offer.digestable_followers.should_not include(@follower)
		end
		it "includes follower which is not a pledger yet" do
			@offer.digestable_followers.should include(@follower)
		end
		it "does not include pledger who is not a follower" do
			@pledger = FactoryGirl.create(:buyer)
			@pledger.pledges << Pledge.new(offer_id: @offer.id, payment_status: :pledged, amount: @offer.max_price)
			@offer.digestable_followers.should_not include(@pledger)
		end
	end
end