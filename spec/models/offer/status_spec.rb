require 'spec_helper'

describe "Offer/Pledge Status" do

	before :all do
		User.destroy_all
		Offer.destroy_all
		Pledge.destroy_all
	end

	before :each do
		#@offer = FactoryGirl.create(:offer, { min_people: 10, max_price: 3500, max_people: 20, min_price: 2500 })
		@offer = FactoryGirl.create(:offer)
		
	end

	after :each do
		User.destroy_all
		Offer.destroy_all
		Pledge.destroy_all
	end

	describe "Pledge Status" do
		
		specify "There should be a single offer and no pledges" do
			Offer.all.count.should == 1
			Pledge.all.count.should == 0
		end
		
		specify "should be Pending when there are not enough pledges at/above this pledge's amount " do
			
			9.times do 
				pledge = FactoryGirl.create(:pledge, { offer_id: @offer.id, amount: @offer.max_price })
			end #times

			@offer.pledges.last.should be_pending
		end #specify

		specify "should be Active when there are enough co-buyers" do
			
			Pledge.all.count.should == 0

			10.times do
				pledge = FactoryGirl.create(:pledge, { offer_id: @offer.id, amount: @offer.max_price })
			end
			@offer.pledges.last.should be_active

		end 

	
	end #describe

	describe "Offer Status" do
		it "should be pending when there is no active pledges" do
			@offer.should be_pending
		end
	
		it "should be active when there is an active pledge" do
			Pledge.all.count.should == 0

			10.times do
				pledge = FactoryGirl.create(:pledge, { offer_id: @offer.id, amount: @offer.max_price })
			end
			
			@offer.should be_active
		end

	end
end #describe
