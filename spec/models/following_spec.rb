require 'spec_helper'

describe Following do

  let(:offer) { FactoryGirl.create(:offer) }
  let(:user) { FactoryGirl.create(:user) }
  let(:following_params) { { offer_id: offer.id, user_id: user.id } }
  let(:following) { Following.create(following_params) }

  it "should traverse its associations in both ways" do
    u = following.user
    o = following.offer
    u.followed_offers.first.should eq o
    o.followers.first.should eq u
  end

  context "when a user already follows an offer" do
    before do
      @user = FactoryGirl.create(:user)
      @offer = FactoryGirl.create(:offer)
      @user.follow(@offer)
    end

    specify "there is only one following" do
      followings = Following.where(user_id: @user.id, offer_id: @offer.id)
      followings.count.should == 1
    end

    it "other following for the same user/offer pair won't be valid" do
      Following.new(user_id: @user.id, offer_id: @offer.id).should_not be_valid
    end

    it "other following for the same user/offer pair won't be created" do
      expect { @user.follow(@offer) }.to raise_error
    end
  end

  it "second instance with same offer/user pair should not be valid" do
    first = following
   
    second = Following.new(following_params)
    
    first.should be_valid
    second.should_not be_valid
  end
end
