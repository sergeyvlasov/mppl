require 'spec_helper'

describe Offer do


	before(:each) do
	   
    Pledge.delete_all
    Offer.delete_all
    User.delete_all

	  @attr = { 
		      
		      :title => "RoR Course",
		      :details => "testing",
		      :min_people => 10,
		      :max_people => 20,
		      :min_price => 2000,
		      :max_price => 4000,
		      :pioneer_discount => 10,
		      :payment_collection_date => "25-5-2012"
		    }

	end
  
  it "should create a new valid offer" do
    Offer.delete_all
    @offer = FactoryGirl.create(:offer)
    @offer.should be_valid
  end
  
  it "should have active status when enough pledges" do	
  	@offer = FactoryGirl.create(:offer,  
  		{ min_people:1,  max_people: 3,  min_price: 20, max_price:50 } 
  		)

    @pledges = [ 
      FactoryGirl.create(:pledge, { offer: @offer, amount: 50} ), 
      FactoryGirl.create(:pledge, { offer: @offer, amount: 50} ) 
    ]
  	@offer.status.should == :active
  end

  it "should give 15 as count of pledges needed for 10, 3500; 20, 2500; 3000" do
  	@offer = FactoryGirl.create(:offer, 
  		{ min_people: 10, max_people: 20, min_price: 2500, max_price:3500 })
  	@offer.min_insiders_count(3000).should == 15
  end

  it "new offer should be in Pending status" do
  	@offer = FactoryGirl.create :offer
  	@offer.status.should == Offer::STATUS_PENDING
  end

  describe "Offer Status Check and Update" do  
    it "Offer Pledges Penging to Active Transition" do
      @offer = FactoryGirl.create(:offer, 
          { min_people: 2, max_price: 6,  
            max_people: 5, min_price: 3 }
        )
        #2->6,3->5,4->4,5->3
        @pledge1 = FactoryGirl.create(:pledge, offer: @offer, amount: 5)
        @pledge2 = FactoryGirl.create(:pledge, offer: @offer, amount: 5)
        @pledge3 = FactoryGirl.create(:pledge, offer: @offer, amount: 5)     
        @pledge4 = FactoryGirl.create(:pledge, offer: @offer, amount: 3)
      
        #@offer.check_and_update_statuses()

        @offer.should be_active
        @pledge1.should be_active

    end
    

    it "Offer Active to Pending Transition" do
        @offer = FactoryGirl.create(:offer, 
        {
          min_people: 4,
          max_people: 8,
          min_price: 40,
          max_price: 59
          });
        
        @pledge1 = FactoryGirl.create(:pledge, offer: @offer, amount: 60)
        @pledge2 = FactoryGirl.create(:pledge, offer: @offer, amount: 59)
        @pledge3 = FactoryGirl.create(:pledge, offer: @offer, amount: 85)
        @pledge4 = FactoryGirl.create(:pledge, offer: @offer, amount: 62)
        @pledge5 = FactoryGirl.create(:pledge, offer: @offer, amount: 51)
        
        @offer.should be_active
        @offer.pledges.first.destroy
        @offer.pledges.last.destroy
        @offer.pledges.count.should == 3
        @offer.check_and_update_statuses
        @offer.should be_pending
    end 
  end
end