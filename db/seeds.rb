# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

admin = FactoryGirl.create :admin_user
puts "New user created: " << admin.name

sellers = []
offers = []
2.times do
	seller = FactoryGirl.create :seller
	sellers << seller
	puts 'New seller created: ' << seller.email

	offer = FactoryGirl.create(:offer, user: seller, published: true, created_at: 3.days.ago, updated_at: 3.days.ago, category_id: 2)
	offers << offer
	puts 'New offer created: ' << offer.title
end

buyers = []
4.times do 
	buyer = FactoryGirl.create(:buyer)
	buyers << buyer
	puts 'New buyer created: ' << buyer.email
end
Category.create name: "Games", parent_id: 0
Category.create name: "Cars", parent_id: 0
Category.create name: "Books", parent_id: 0
Category.create name: "Movies", parent_id: 0
Category.create name: "ComputerGames", parent_id: 1
Category.create name: "TableGames", parent_id: 1
Category.create name: "RunningCars", parent_id: 2
Category.create name: "ElegantCars", parent_id: 2
Category.create name: "YellowBooks", parent_id: 3
Category.create name: "PinkBooks", parent_id: 3
Category.create name: "ThrillerMovie", parent_id: 4
Category.create name: "HorrorMovie", parent_id: 4
Category.create name: "Sport", parent_id: nil

Pledge.create([
	{ user_id: buyers[0].id, offer_id: offers[0].id, amount: offers[0].max_price, payment_status: :pledged }, 
	{ user_id: buyers[0].id, offer_id: offers[0].id, amount: offers[0].max_price, payment_status: :pledged, created_at: 2.days.ago },
	{ user_id: buyers[1].id, offer_id: offers[0].id, amount: offers[0].max_price, payment_status: :pledged }, 
	{ user_id: buyers[2].id, offer_id: offers[0].id, amount: offers[0].max_price, payment_status: :pledged, created_at: 2.days.ago },
	{ user_id: buyers[2].id, offer_id: offers[1].id, amount: offers[1].max_price, payment_status: :pledged }, 
	{ user_id: buyers[3].id, offer_id: offers[1].id, amount: offers[1].max_price, payment_status: :pledged, created_at: 2.days.ago }
	])





Offer.create user_id:1, title:"Diablo", min_people: 10, max_people:20, min_price:1300, max_price: 2000, image_url: "http://arodgersblog.files.wordpress.com/2012/06/d15.jpg", published: true, payment_collection_date:"10/8/2012", category_id:5

Offer.create user_id:1, title:"Rayman", min_people: 10, max_people:20, min_price:1300, max_price: 2000, image_url: "https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcTgNUFwlQy0EVq_EXODcB9VTlwuikS6Mt1qJbx9oF2jVsTw1jPO", published: true, payment_collection_date:"10/8/2012", category_id:5

Offer.create user_id:1, title:"Monopoly", min_people: 10, max_people:20, min_price:1300, max_price: 2000, image_url: "http://www.hasbro.com/common/productimages/en_US/8ee05ccf6d4010148bf09efbf894f9d4/ADE5020F19B9F369107AA9391334FB07.jpg", published: true, payment_collection_date:"10/8/2012", category_id:6

Offer.create user_id:1, title:"Roulette", min_people: 10, max_people:20, min_price:1300, max_price: 2000, image_url: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ_O4T839DNcRxGmJBothj5SUhhnKqMXDtwQzZkhG6oxYvyerZu7xpWhw", published: true, payment_collection_date:"10/8/2012", category_id:6

Offer.create user_id:1, title:"Ferrari", min_people: 10, max_people:20, min_price:1300,  max_price: 2000, image_url: "http://www.prestigeluxuryrentals.com/blog/wp-content/uploads/2012/02/Ferrari-Make-Record-Sales-and-Revenue-in-2011.jpg", published: true, payment_collection_date:"10/8/2012", category_id:7

Offer.create user_id:1, title:"Audi TT", min_people: 10, max_people:20, min_price:1300, max_price: 2000, image_url: "http://www.autoinfoz.com/gallery/exterior/big/ext-1309177540Audi-TT-RS2010.jpg", published: true, payment_collection_date:"10/8/2012", category_id:7

Offer.create user_id:1, title:"Jaguar Daimler", min_people: 10, max_people:20, min_price:1300, max_price: 2000, image_url: "http://img.automobile.de/modellbilder/Jaguar-Daimler-14993_1122545393110.jpg", published: true, payment_collection_date:"10/8/2012", category_id:8

Offer.create user_id:1, title:"Mercedes e", min_people: 10, max_people:20, min_price:1300, max_price: 2000, image_url: "http://static.blogo.it/autoblog/carlsson-mercedes-classe-e/Carlsson_EKlasse_W212_01.jpg", published: true, payment_collection_date:"10/8/2012", category_id:8


Offer.create user_id:1, title:"Da Vinci Code", min_people: 10, max_people:20, min_price:1300, max_price: 2000, image_url: "http://brandsandfilms.com/wp-content/uploads/2010/06/DanBrown_TheDaVinciCode.jpg", published: true, payment_collection_date:"10/8/2012", category_id:9

Offer.create user_id:1, title:"Northern Lights", min_people: 10, max_people:20, min_price:1300, max_price: 2000, image_url: "http://newsimg.bbc.co.uk/media/images/42409000/jpg/_42409756_northern_lights_300.jpg", published: true, max_price: 2000, payment_collection_date:"10/8/2012", category_id:9

Offer.create user_id:1, title:"Love In Love ", min_people: 10, max_people:20, min_price:1300, max_price: 2000, image_url: "http://www.fatwallet.com/static/attachments/190881_book1.jpg", published: true, max_price: 2000, payment_collection_date:"10/8/2012", category_id:10

Offer.create user_id:1, title:"Love and Lovers", min_people: 10, max_people:20, min_price:1300, max_price: 2000, image_url: "http://cefn.com/blog/photos/open_book.jpg", published: true, max_price: 2000, payment_collection_date:"10/8/2012", category_id:10

Offer.create user_id:1, title:"The Happening", min_people: 10, max_people:20, min_price:1300, max_price: 2000, image_url: "http://www.firstshowing.net/img/happening-poster-big2.jpg",published: true, payment_collection_date:"10/8/2012", category_id:11

Offer.create user_id:1, title:"The Box", min_people: 10, max_people:20, min_price:1300, max_price: 2000, image_url: "http://www.atomicpopcorn.net/wp-content/uploads/2009/11/movie_TheBox.jpg", published: true, payment_collection_date:"10/8/2012", category_id:11

Offer.create user_id:1, title:"Watcher", min_people: 10, max_people:20, min_price:1300, max_price: 2000, image_url: "http://4.bp.blogspot.com/_2Z7VYcC8u4c/SPR3t0yWYyI/AAAAAAAADHU/KbLbSsewsLw/s400/horror+movie+dvd+cover.gif", published: true, payment_collection_date:"10/8/2012", category_id:12

Offer.create user_id:1, title:"The Wake Wood", min_people: 10, max_people:20, min_price:1300, max_price: 2000, image_url: "http://4.bp.blogspot.com/-oabdEl53IGA/TZKD6Nn2ZLI/AAAAAAAAAns/lwiDYZdbKg0/s400/The+Wake+Wood.jpg", published: true, payment_collection_date:"10/8/2012", category_id:12

Offer.create user_id:1, title:"Ping Pong", min_people: 10, max_people:20, min_price:1300, max_price: 2000, image_url: "http://www.ispot.co.il/admin/uploads/prev_647.jpg", published: true, payment_collection_date:"10/8/2012", category_id:1

Offer.create user_id:1, title:"Basketball training", min_people: 10, max_people:20, min_price:1300, max_price: 2000, image_url: "http://images.nationalgeographic.com/wpf/media-live/photos/000/143/cache/basketball_14306_600x450.jpg", published: true, payment_collection_date:"10/8/2012", category_id:13


puts "everything done"