class AddPayerEmailToPaymentNotifications < ActiveRecord::Migration
  def change
    add_column :payment_notifications, :payer_email, :string
  end
end
