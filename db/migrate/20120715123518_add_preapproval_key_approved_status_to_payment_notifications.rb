class AddPreapprovalKeyApprovedStatusToPaymentNotifications < ActiveRecord::Migration
  def change
    add_column :payment_notifications, :preapproval_key, :string
    add_column :payment_notifications, :approved, :boolean
    add_column :payment_notifications, :status, :string, limit: 40
  end
end
