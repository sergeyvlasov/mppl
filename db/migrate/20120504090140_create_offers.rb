class CreateOffers < ActiveRecord::Migration
  def change
    create_table :offers do |t|
      t.references :user
      t.string     :title, limit: 80
      t.text       :details
      t.string     :currency, limit: 10
      t.integer    :min_people, null: false, default: 1
      t.integer    :max_people, null: false, default: 2
      t.integer    :min_price, null: false, default: 1
      t.integer    :max_price, null: false, default: 2
      t.integer    :pioneer_discount, default: 0
      t.date       :payment_collection_date
      t.string     :status,   :default => "Pending", limit: 10
      t.float      :current_price
      t.boolean    :published, default: false
      t.string     :image_url, limit: 255
      t.timestamps
    end
    add_index :offers, :user_id
  end
end