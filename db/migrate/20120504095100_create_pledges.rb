class CreatePledges < ActiveRecord::Migration
  def change
    create_table :pledges do |t|
      t.references :offer
      t.references :user
      t.integer    :amount, null: false
      t.string     :preapproval_key, limit: 40
      t.string     :auth, limit: 40
      t.string     :status, :default => "NA", limit: 20
      t.integer    :payment_status, default: 0
      t.timestamps
    end
    add_index :pledges, :offer_id
    add_index :pledges, :user_id
  end
end


