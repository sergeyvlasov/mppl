class AddWebsiteToOffer < ActiveRecord::Migration
  def change
    add_column :offers, :website, :string, limit: 140
  end
end
