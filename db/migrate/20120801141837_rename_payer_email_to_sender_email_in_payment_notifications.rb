class RenamePayerEmailToSenderEmailInPaymentNotifications < ActiveRecord::Migration
  def up
  	rename_column(:payment_notifications, :payer_email, :sender_email)
  end

  def down
  	rename_column(:payment_notifications, :sender_email, :payer_email)
  end
end
