class CreatePayments < ActiveRecord::Migration
  def change
    create_table :payments do |t|
      t.references 	:pledge
      t.integer 	:amount
      t.string 		:currency, limit: 10
	    t.date     :timestamp
	    t.string   :ack
	    t.string   :correlationId
	    t.integer  :build
	    t.string   :paykey
	    t.string   :payment_exec_status
	    t.integer  :offer_id

      t.timestamps
    end
    add_index :payments, :pledge_id
  end
end

