class CreateFollowings < ActiveRecord::Migration
  def change
    create_table :followings do |t|
      t.references :user
      t.references :offer

      t.timestamps
    end
    add_index :followings, :user_id
    add_index :followings, :offer_id
  end
end
