# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20121014091407) do

  create_table "categories", :force => true do |t|
    t.string   "name"
    t.integer  "parent_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "followings", :force => true do |t|
    t.integer  "user_id"
    t.integer  "offer_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "followings", ["offer_id"], :name => "index_followings_on_offer_id"
  add_index "followings", ["user_id"], :name => "index_followings_on_user_id"

  create_table "offers", :force => true do |t|
    t.integer  "user_id"
    t.string   "title",                   :limit => 80
    t.text     "details"
    t.string   "currency",                :limit => 10
    t.integer  "min_people",                             :default => 1,         :null => false
    t.integer  "max_people",                             :default => 2,         :null => false
    t.integer  "min_price",                              :default => 1,         :null => false
    t.integer  "max_price",                              :default => 2,         :null => false
    t.integer  "pioneer_discount",                       :default => 0
    t.date     "payment_collection_date"
    t.string   "status",                  :limit => 10,  :default => "Pending"
    t.float    "current_price"
    t.boolean  "published",                              :default => false
    t.string   "image_url"
    t.datetime "created_at",                                                    :null => false
    t.datetime "updated_at",                                                    :null => false
    t.string   "website",                 :limit => 140
    t.integer  "category_id"
  end

  add_index "offers", ["user_id"], :name => "index_offers_on_user_id"

  create_table "payment_notifications", :force => true do |t|
    t.text     "params"
    t.datetime "created_at",                    :null => false
    t.datetime "updated_at",                    :null => false
    t.string   "preapproval_key"
    t.boolean  "approved"
    t.string   "status",          :limit => 40
    t.string   "sender_email"
  end

  create_table "payments", :force => true do |t|
    t.integer  "pledge_id"
    t.integer  "amount"
    t.string   "currency",            :limit => 10
    t.date     "timestamp"
    t.string   "ack"
    t.string   "correlationId"
    t.integer  "build"
    t.string   "paykey"
    t.string   "payment_exec_status"
    t.integer  "offer_id"
    t.datetime "created_at",                        :null => false
    t.datetime "updated_at",                        :null => false
  end

  add_index "payments", ["pledge_id"], :name => "index_payments_on_pledge_id"

  create_table "pledges", :force => true do |t|
    t.integer  "offer_id"
    t.integer  "user_id"
    t.integer  "amount",                                          :null => false
    t.string   "preapproval_key", :limit => 40
    t.string   "auth",            :limit => 40
    t.string   "status",          :limit => 20, :default => "NA"
    t.integer  "payment_status",                :default => 0
    t.datetime "created_at",                                      :null => false
    t.datetime "updated_at",                                      :null => false
  end

  add_index "pledges", ["offer_id"], :name => "index_pledges_on_offer_id"
  add_index "pledges", ["user_id"], :name => "index_pledges_on_user_id"

  create_table "roles", :force => true do |t|
    t.string   "name"
    t.integer  "resource_id"
    t.string   "resource_type"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
  end

  add_index "roles", ["name", "resource_type", "resource_id"], :name => "index_roles_on_name_and_resource_type_and_resource_id"
  add_index "roles", ["name"], :name => "index_roles_on_name"

  create_table "users", :force => true do |t|
    t.string   "email",                  :default => "", :null => false
    t.string   "encrypted_password",     :default => "", :null => false
    t.string   "name"
    t.string   "paypal"
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          :default => 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                             :null => false
    t.datetime "updated_at",                             :null => false
  end

  add_index "users", ["email"], :name => "index_users_on_email", :unique => true
  add_index "users", ["reset_password_token"], :name => "index_users_on_reset_password_token", :unique => true

  create_table "users_roles", :id => false, :force => true do |t|
    t.integer "user_id"
    t.integer "role_id"
  end

  add_index "users_roles", ["user_id", "role_id"], :name => "index_users_roles_on_user_id_and_role_id"

end
