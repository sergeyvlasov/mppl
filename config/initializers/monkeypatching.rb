class Fixnum
	def single?
		self == 1
	end
end

def pluralize num, word
	ActionController::Base.helpers.pluralize num, word
end

