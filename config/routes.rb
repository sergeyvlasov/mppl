MPPL::Application.routes.draw do

  
  post "paypal/pledge"

  get "paypal/redirect_to_paypal"
  
  scope "(:locale)", :locale => /en|he|it/ do
  
    resources :widgets do
      collection do
        get :translations
        get :faq
      end

      member do
        get :preview
        get :embed
        get :widget
        get :snippet
        get :snippet_mobile_portrait #320x240
        get :snippet_mobile_landscape #480x320
        get :snippet_small_tablet_portrait #600x800
        get :snippet_small_tablet_landscape #800x600
        get :snippet_tablet_portrait #768x1024
        get :snipppet_tablet_landscape #(1024x768)
      end
    end
    resources :categories
    resources :payment_notifications
    
      resources :offers do
  	    resources :pledges do
          member do
            post :pay
            get :paypal_details
          end
        end 
        resources :payments
  
        collection do
          get :chart
        end
        member do
          get :chart
          get :thankyou
          get :accept_cash
          post :receive_cash
          post :add_follower
          post :remove_follower
          post :contact_us
        end
      end
    
  end
  
  

  match '/:locale' => 'home#index'
  root :to => "home#index"


  
  
  scope "(:locale)", :locale => /en|he|it/ do devise_for :users 
  resources :users, :only => [:show, :index] 
   
  
  match 'my_offers', to: "offers#my", as: :my_offers
  match 'my_pledges', to: "pledges#my", as: :my_pledges

  match 'explore', to: "home#explore", as: :explore

  match 'faq', to: "home#faq", as: :faq

  match 'about', to: "payment_notifications#create", via: :post, as: :old_ipn_listener
  match 'about', to: "home#about", via: :get, as: :about

  match 'privacy', to: "home#privacy-policy", as: :privacy
  match 'terms', to: "home#terms-of-use", as: :terms
  match 'how-it-works', to: "home#how_it_works", as: :how_it_works

  match 'pledges', to: "pledges#all", as: :pledges_all
  match 'payments', to: "payments#all", as: :payments_all

  match 'preapproval_return/:id(/:old_pledge_id)', to: "pledges#preapproval_return", via: :get, as: :preapproval_return
  match 'preapproval_cancel/:id(/:old_pledge_id)', to: "pledges#preapproval_cancel", via: :get, as: :preapproval_cancel
  
  end


end
