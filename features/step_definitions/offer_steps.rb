
def create_an_offer 
	offer = FactoryGirl.create(:offer)
  	offer.should_not be_nil
  	offer.should be_valid

end

Then /^Offer with name "(.*?)" should be exist$/ do |title|
  offer = Offer.find_by_title(title)
  offer.should_not be_nil
end

Given /^I am a visitor$/ do
  pending # express the regexp above with the code you wish you had
end

Given /^I have my default offer with title "(.*?)"$/ do |arg1|
  pending # express the regexp above with the code you wish you had
end

Given /^There is an offer$/ do
  create_an_offer
end

Given /^There is another offer$/ do
  create_an_offer
end

Given /^There is an offer with title "(.*?)"$/ do |title|
  offer = FactoryGirl.create(:offer, title: title) #Offer.find_or_create_by_title title
  offer.should_not be_nil
  offer.should be_valid
end

Then /^I should see "(.*?)" with (\d+) offers listed on it$/ do |title, offer_count|
  
end

When /^\.\.\.$/ do
  pending # express the regexp above with the code you wish you had
end