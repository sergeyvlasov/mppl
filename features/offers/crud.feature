@offer
Feature: Offer
  In order to earn money
  An authenticated user
  Should be able to create and manage offers

  #CREATE
  Scenario: Admin user creates Offer
    Given I am logged in
     When I follow "Offers" 
      And I follow "Create An Offer"
      And I fill in the following:
         | Title                   | My Offer |
         | Details                 | My Course |
         | Min people              | 10 |
         | Max people              | 100 |
         | Min price               | 1000 |
         | Max price               | 2000 |
         | Payment collection date | 06/10/2012 |
      And I press "Create Offer"
     Then Offer with name "My Offer" should be exist
      And I should see "Offer was successfully created."
      

  #READ (INDEX)
  @offer_index
  Scenario: Visitor Observes Offer List
   Given There is an offer
    When I visit home page
    Then I should see "Offers"
    And show me the page
     

  #READ (SHOW)
  @wip
  Scenario: Visitor Observes Offer List
    Given I am logged in
      And I have my default offer with title "My Great Offer"
     When I follow "Offers"
     Then I should see "Offers Page"
     And show me the page 

  #UPDATE
  @wip
  Scenario: Signed up user edit existing Offer
    Given I am logged in
      And I have my default offer with title "My Great Offer"
     When I follow "Offers"
     Then I should see "Offers Page"
     And show me the page 

  #DELETE
  @wip
  Scenario: Signed up user delete existing Offer
    Given I am logged in
      And I have my default offer with title "My Great Offer"
     When I follow "Offers"
      And ...

